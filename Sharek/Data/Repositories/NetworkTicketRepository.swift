//
//  NetworkTicketRepository.swift
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import Alamofire
import Gloss

class NetworkTicketRepository : TicketRepository {
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mUserDefault = UserDefault()
    
    enum ErrorType {
        case none
        case auth
        case network
        case serverError
    }
    
    
    
    func addTicket(lat: String, lang: String, Location:String,  imagesUrl : [String:Int], desc:String, ticketEmotionId:Int,deviceToken:String, completionHandler: @escaping (_ resultData : Int, _ error : NetworkTicketRepository.ErrorType) -> ())
    {
        
        let   parameters = ["deviceToken": mUserDefault.getOneSignalID()!, //appDelegate.getUUID()
                            "description": desc,
                            "land": lang,
                            "ticketEmotionId": ticketEmotionId,
                            "lat": lat,
                            "location": Location,
                            "imagesUrl": imagesUrl] as [String : Any]
        
        
        
        Alamofire.request(APIURLs.MAIN_URL + APIURLs.ADD_TICKET, method: .post, parameters: parameters)
            .responseJSON { response in
                switch response.result {
                case .success(_):
                    let addTicketResponse = AddTicketResponse(json: response.result.value! as! JSON)!
                    print(response.result.value!)
                    
                    if(response.response?.statusCode == 200 && addTicketResponse.error?.message == "success") {
                        completionHandler((addTicketResponse.data?.ticketId)!, NetworkTicketRepository.ErrorType.none)
                    } else {
                        completionHandler(0, NetworkTicketRepository.ErrorType.serverError)
                    }
                    
                case .failure(_):
                    
                    completionHandler(0, NetworkTicketRepository.ErrorType.network)
                    print("failure ely gowa result")
                }
        }
    }
    
    
    func uploadImage(imageFile:UIImage?, completionHandler: @escaping (_ resultData : Int, _ error : NetworkTicketRepository.ErrorType) -> ())
    {
        
        if(imageFile != nil) {
            
            let URL = try! URLRequest(url: APIURLs.MAIN_URL + APIURLs.UPLOAD_IMAGE, method: .post)
            Alamofire.upload(multipartFormData: {
                
                (multipartFormData) in
                
                if let imageData = UIImageJPEGRepresentation(imageFile!, 0.6) {
                    multipartFormData.append(imageData, withName: "imageFileUpload", fileName: "image.jpg", mimeType: "image/jpg")
                }
                
                
            }, with: URL, encodingCompletion: {
                
                (encodingResult) in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON {
                        response in
                        switch response.result {
                        case .success(_):
                            
                            let uploadImageResponse = UploadImageResponse(json: response.result.value! as! JSON)!
                            print(response.result.value!)
                            if(response.response?.statusCode == 200) {
                                
                                completionHandler((uploadImageResponse.data?.imageId)!, NetworkTicketRepository.ErrorType.none)
                                
                            } else {
                                completionHandler(0, NetworkTicketRepository.ErrorType.serverError)
                            }
                            
                        case .failure(_):
                            print("failure ely gowa result")
                            
                            completionHandler(0, NetworkTicketRepository.ErrorType.network)
                            
                        }
                        
                    }
                case .failure(_):
                    print("failure el kbera")
                    completionHandler(0, NetworkTicketRepository.ErrorType.network)
                }
            })
            
            
        }
        
    }
    
    
    func deleteImage(imageId:String!, completionHandler: @escaping (_ resultData : Bool, _ error : NetworkTicketRepository.ErrorType) -> ())
    {
        
        let   parameters = [ "imageId": imageId] as [String : Any]
        
        Alamofire.request(APIURLs.MAIN_URL + APIURLs.DELETE_IMAGE, method: .post, parameters: parameters)
            .responseJSON { response in
                switch response.result {
                case .success(_):
                    let addTicketResponse = AddTicketResponse(json: response.result.value! as! JSON)!
                    print(response.result.value!)
                    
                    if(response.response?.statusCode == 200 && addTicketResponse.error?.message == "success") {
                        completionHandler(true, NetworkTicketRepository.ErrorType.none)
                    } else {
                        
                        completionHandler(false, NetworkTicketRepository.ErrorType.serverError)
                    }
                    
                case .failure(_):
                    
                    completionHandler(false, NetworkTicketRepository.ErrorType.network)
                    print("failure ely gowa result")
                }
        }
        
    }
    
    //get all tickets method
    
    func getAllTickets(deviceToken:String, completionHandler: @escaping (_ resultData : AllTicketsData, _ error : NetworkTicketRepository.ErrorType) -> ()){
        
        let   parameters = ["deviceToken" : deviceToken]
        
        Alamofire.request(APIURLs.MAIN_URL + APIURLs.GET_TICKETS, method: .post, parameters: parameters)
            .responseJSON { response in
                switch response.result {
                case .success(_):
                    
                    let getTicketsResponse = GetAllTicketsResponse(json: response.result.value! as! JSON)!
                    print(response.result.value!)
                    if(getTicketsResponse.error?.message == "success") {
                        if getTicketsResponse.data != nil{
                            completionHandler(getTicketsResponse.data!, NetworkTicketRepository.ErrorType.none)
                        }else{
                           completionHandler(AllTicketsData(), NetworkTicketRepository.ErrorType.none)
                        }
                    }else{
                        completionHandler(AllTicketsData(), NetworkTicketRepository.ErrorType.serverError)
                    }
                    
                    
                case .failure(_):
                    
                    print("failure el kbera")
                    completionHandler(AllTicketsData(), NetworkTicketRepository.ErrorType.network)
                    
                }
        }
        
    }
    
    func markAsSeen(deviceToken:String , ticketId : Int, completionHandler: @escaping (_ resultData : Bool, _ error : NetworkTicketRepository.ErrorType) -> ()){
        
        let   parameters = [ "deviceToken": deviceToken, "ticketId": ticketId] as [String : Any]
        
        Alamofire.request(APIURLs.MAIN_URL + APIURLs.MARK_AS_SEEN, method: .post, parameters: parameters)
            .responseJSON { response in
                switch response.result {
                case .success(_):
                    let markAsSeenResponse = DeleteImageResponse(json: response.result.value! as! JSON)!
                    print(response.result.value!)
                    
                    if(response.response?.statusCode == 200 && markAsSeenResponse.error?.message == "success") {
                        completionHandler(true, NetworkTicketRepository.ErrorType.none)
                    } else {
                        
                        completionHandler(false, NetworkTicketRepository.ErrorType.serverError)
                    }
                    
                case .failure(_):
                    
                    completionHandler(false, NetworkTicketRepository.ErrorType.network)
                    print("failure ely gowa result")
                }
        }
    }
    
    
    func getRelatedTickets(baseTicketId:String , completionHandler: @escaping (_ resultData : [Ticket], _ error : NetworkTicketRepository.ErrorType) -> ()){
        
        let   parameters = [ "baseTicketId": baseTicketId] as [String : Any]
        
        Alamofire.request(APIURLs.MAIN_URL + APIURLs.RELATED_TICKETS, method: .post, parameters: parameters)
            .responseJSON { response in
                switch response.result {
                case .success(_):
                    
                    let relatedTicketsResponse = RelatedTicketsResponse(json: response.result.value! as! JSON)!
                    print(response.result.value!)
                    
                    if(response.response?.statusCode == 200 && relatedTicketsResponse.error?.message == "success") {
                        completionHandler(relatedTicketsResponse.data!, NetworkTicketRepository.ErrorType.none)
                    } else {
                        
                        completionHandler([Ticket](), NetworkTicketRepository.ErrorType.serverError)
                    }
                case .failure(_):
                    
                    completionHandler([Ticket](), NetworkTicketRepository.ErrorType.network)
                }
        }
        
    }
    
    
    func searchTicketNo(deviceToken:String , ticketIdPart : String, completionHandler: @escaping (_ resultData : [Ticket], _ error : NetworkTicketRepository.ErrorType) -> ()){
        
        let   parameters = [ "deviceToken": deviceToken, "ticketIdPart": ticketIdPart] as [String : Any]
        
        Alamofire.request(APIURLs.MAIN_URL + APIURLs.SEARCH, method: .post, parameters: parameters)
            .responseJSON { response in
                switch response.result {
                case .success(_):
                    let relatedTicketsResponse = RelatedTicketsResponse(json: response.result.value! as! JSON)!
                    print(response.result.value!)
                    
                    if(response.response?.statusCode == 200 && relatedTicketsResponse.error?.message == "success") {
                        completionHandler(relatedTicketsResponse.data!, NetworkTicketRepository.ErrorType.none)
                    } else {
                        
                        completionHandler([Ticket](), NetworkTicketRepository.ErrorType.serverError)
                    }
                case .failure(_):
                     completionHandler([Ticket](), NetworkTicketRepository.ErrorType.network)
                    print("failure ely gowa result")
                }
        }
    }
    
    func changeUserId(deviceToken:String , newDeviceToken : String, completionHandler: @escaping (_ resultData : Bool, _ error : NetworkTicketRepository.ErrorType) -> ()){
        let   parameters = [ "deviceToken": deviceToken, "newDeviceToken": newDeviceToken] as [String : Any]
        print("deviceToken ",deviceToken)
        print("newdeviceToken ",newDeviceToken)
        Alamofire.request(APIURLs.MAIN_URL + APIURLs.CHANGE_USERID, method: .post, parameters: parameters)
            .responseJSON { response in
                switch response.result {
                case .success(_):
                    let markAsSeenResponse = DeleteImageResponse(json: response.result.value! as! JSON)!
                    print(response.result.value!)
                    
                    if(response.response?.statusCode == 200 && markAsSeenResponse.error?.message == "success") {
                        completionHandler(true, NetworkTicketRepository.ErrorType.none)
                    } else {
                        
                        completionHandler(false, NetworkTicketRepository.ErrorType.serverError)
                    }
                    
                case .failure(_):
                    
                    completionHandler(false, NetworkTicketRepository.ErrorType.network)
                    print("failure ely gowa result")
                }
        }
    }
}


