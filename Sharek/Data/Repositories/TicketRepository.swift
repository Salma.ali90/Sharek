//
//  TicketRepository.swift
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import UIKit

protocol TicketRepository {
    
    func addTicket(lat: String, lang: String, Location:String, imagesUrl : [String:Int], desc:String, ticketEmotionId:Int,deviceToken:String, completionHandler: @escaping (_ resultData : Int, _ error : NetworkTicketRepository.ErrorType) -> ())
    
    func uploadImage(imageFile:UIImage?, completionHandler: @escaping (_ resultData : Int, _ error : NetworkTicketRepository.ErrorType) -> ())
    
    func deleteImage(imageId:String!, completionHandler: @escaping (_ resultData : Bool, _ error : NetworkTicketRepository.ErrorType) -> ())
    
    
    func getAllTickets(deviceToken:String, completionHandler: @escaping (_ resultData : AllTicketsData, _ error : NetworkTicketRepository.ErrorType) -> ())
    
    func markAsSeen(deviceToken:String , ticketId : Int, completionHandler: @escaping (_ resultData : Bool, _ error : NetworkTicketRepository.ErrorType) -> ())
    
    func getRelatedTickets(baseTicketId:String , completionHandler: @escaping (_ resultData : [Ticket], _ error : NetworkTicketRepository.ErrorType) -> ())
    
    func searchTicketNo(deviceToken:String , ticketIdPart : String, completionHandler: @escaping (_ resultData : [Ticket], _ error : NetworkTicketRepository.ErrorType) -> ())
    
    func changeUserId(deviceToken:String , newDeviceToken : String, completionHandler: @escaping (_ resultData : Bool, _ error : NetworkTicketRepository.ErrorType) -> ())

}
