//
//  post.swift
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import Gloss

class Ticket:  NSObject , NSCoding, Glossy{
    

    var  ticketId : Int?
    var ticketDate : String?
    var relatedTickestNo : Int?
    var  replay : [TicketReply]?
    var lat: String?
    var lang: String?
    var Location:String?
    var imagesUrl : [String]?
    var desc:String?
    var ticketEmotionId:Int?
    //var ticketEmotionId:String?
    var deviceToken:String?
    
    
    
    override init() {
        lat = ""
        lang = ""
        Location = ""
        imagesUrl = [String]()
        desc = ""
        ticketEmotionId = -2
        deviceToken = ""
        ticketId = 0
        ticketDate = ""
        relatedTickestNo = 0
        replay = [TicketReply]()
        
    }
    
    init(lat: String?, lang: String?, Location:String?, imagesUrl : [String]?, desc:String?, ticketEmotionId:Int?,deviceToken:String?,ticketId:Int?,ticketDate:String?,relatedTickestNo:Int?,replay:[TicketReply]?){
        self.lat = lat
        self.lang = lang
        self.Location = Location
        self.imagesUrl = imagesUrl
        self.desc = desc
        self.ticketEmotionId = ticketEmotionId
        self.deviceToken = deviceToken
        self.ticketId = ticketId
        self.ticketDate = ticketDate
        self.relatedTickestNo = relatedTickestNo
        self.replay = replay

    }
    
    required init?(json : JSON){
        lat = Decoder.decode(key: "lat")(json)
        lang = Decoder.decode(key: "lang")(json)
        Location = Decoder.decode(key: "location")(json)
        imagesUrl = Decoder.decode(key: "imagesUrl")(json)
        desc = Decoder.decode(key: "description")(json)
        ticketEmotionId = Decoder.decode(key: "ticketEmotionId")(json)
        deviceToken = Decoder.decode(key: "deviceToken")(json)
        ticketId = Decoder.decode(key: "ticketId")(json)
        ticketDate = Decoder.decode(key: "ticketDate")(json)
        relatedTickestNo = Decoder.decode(key: "relatedTickestNo")(json)
        replay = Decoder.decode(decodableArrayForKey: "replay")(json)
        
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            Encoder.encode(key: "lat")(self.lat),
            Encoder.encode(key: "lang")(self.lang),
            Encoder.encode(key: "location")(self.Location),
            Encoder.encode(key: "imagesUrl")(self.imagesUrl),
            Encoder.encode(key: "description")(self.desc),
            Encoder.encode(key: "ticketEmotionId")(self.ticketEmotionId),
            Encoder.encode(key: "deviceToken")(self.deviceToken),
            Encoder.encode(key: "ticketId")(self.ticketId),
            Encoder.encode(key: "ticketDate")(self.ticketDate),
            Encoder.encode(key: "relatedTickestNo")(self.relatedTickestNo),
            Encoder.encode(key: "replay")(self.replay)
            
            
            ])
    }
    required convenience init(coder aDecoder: NSCoder) {
        let lat = aDecoder.decodeObject(forKey: "lat") as! String
        let lang = aDecoder.decodeObject(forKey: "lang") as? String
        let Location = aDecoder.decodeObject(forKey: "location") as? String
        let imagesUrl = aDecoder.decodeObject(forKey: "imagesUrl") as? [String]
        let desc = aDecoder.decodeObject(forKey: "description") as? String
        let ticketEmotionId = aDecoder.decodeObject(forKey: "ticketEmotionId") as? Int
        let deviceToken = aDecoder.decodeObject(forKey: "deviceToken") as? String
        let ticketId = aDecoder.decodeObject(forKey: "ticketId") as? Int
        let ticketDate = aDecoder.decodeObject(forKey: "ticketDate") as? String
        let relatedTickestNo = aDecoder.decodeObject(forKey: "relatedTickestNo") as? Int
        let replay = aDecoder.decodeObject(forKey: "replay") as? [TicketReply]
        
    
        self.init(lat: lat, lang: lang, Location: Location, imagesUrl: imagesUrl, desc: desc, ticketEmotionId: ticketEmotionId, deviceToken: deviceToken,ticketId : ticketId , ticketDate : ticketDate , relatedTickestNo : relatedTickestNo , replay : replay)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(lat, forKey: "lat")
        aCoder.encode(lang, forKey: "lang")
        aCoder.encode(Location, forKey: "location")
        aCoder.encode(imagesUrl, forKey: "imagesUrl")
        aCoder.encode(desc, forKey: "description")
        aCoder.encode(ticketEmotionId, forKey: "ticketEmotionId")
        aCoder.encode(deviceToken, forKey: "deviceToken")
        aCoder.encode(ticketId, forKey: "ticketId")
        aCoder.encode(ticketDate, forKey: "ticketDate")
        aCoder.encode(relatedTickestNo, forKey: "relatedTickestNo")
        aCoder.encode(replay, forKey: "replay")
        
    }
    
    
}
