//
//  UploadImageResponse.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/9/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation

import  Gloss

class UploadImageResponse : Gloss.Decodable{
    
    
    var error : ErrorResponse?
    var data : ImageDataResponse?
    
    required init?(json: JSON) {
        self.error = Decoder.decode(decodableForKey: "error")(json)!
        self.data = Decoder.decode(decodableForKey: "data")(json)
        
    }

    
    
        
}
