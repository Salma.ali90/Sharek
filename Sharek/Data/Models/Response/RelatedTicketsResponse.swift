//
//  RelatedTicketsResponse.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/17/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import Gloss

class RelatedTicketsResponse: Gloss.Decodable{
    
    var error : ErrorResponse?
    var data : [Ticket]?
    
    required init?(json: JSON) {
        self.error = Decoder.decode(decodableForKey: "error")(json)!
        self.data = Decoder.decode(decodableArrayForKey: "data")(json)
        
    }
    
}
