//
//  AddTicketResponse.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/16/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation

import  Gloss

class AddTicketResponse : Gloss.Decodable{
    
    
    var error : ErrorResponse?
    var data : AddTicketResponseData?
    
    required init?(json: JSON) {
        self.error = Decoder.decode(decodableForKey: "error")(json)!
        self.data = Decoder.decode(decodableForKey: "data")(json)
        
    }
    
    
    
    
}

