//
//  DeleteImageResponse.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/16/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation

import  Gloss

class DeleteImageResponse : Gloss.Decodable{
    
    
    var error : ErrorResponse?
    
    required init?(json: JSON) {
        self.error = Decoder.decode(decodableForKey: "error")(json)!
        
        
    }
    
    
    
    
}
