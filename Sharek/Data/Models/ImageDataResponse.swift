//
//  ImageDataResponse.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/16/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import Gloss

class ImageDataResponse : NSObject,Glossy{
    
    var imageId : Int
    
    required init?(json: JSON) {
        
        self.imageId = Decoder.decode(key:"imageId")(json)!
        
    }
    override init() {
        
        imageId = 0
    }
    
    
    
    func toJSON() -> JSON? {
        return jsonify([
            
            Encoder.encode(key: "imageId")(self.imageId)
            
            ])
    }
    
    
}
