//
//  ErrorResponse.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/13/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import Gloss

class ErrorResponse : Glossy{


    var  status : Bool
    var message : String
    
    init() {
      
        status = false
        message = ""
    }
    
    init(status: Bool,message: String) {
       
        self.status = status
        self.message = message
        
    }
    
    required init?(json : JSON){

        status = Decoder.decode(key: "status")(json)!
        message = Decoder.decode(key: "message")(json)!
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
        
            Encoder.encode(key: "status")(self.status),
            Encoder.encode(key: "message")(self.message)
            
            ])
    }

}
