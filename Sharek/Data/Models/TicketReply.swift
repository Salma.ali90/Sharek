//
//  TicketReplay.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/13/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import Gloss

class TicketReply: Glossy {
    
    var replay: String?
    var replayDate: String?
    
    
     init() {
        replay = ""
        replayDate = ""
        
        
    }
    
    init(replay: String, replayDate: String,image:String ) {
        self.replay = replay
        self.replayDate = replayDate
       
        
    }
    
    required init?(json : JSON){
        replay = Decoder.decode(key: "replay")(json)
        replayDate = Decoder.decode(key: "replayDate")(json)
        
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            Encoder.encode(key: "replay")(self.replay),
            Encoder.encode(key: "replayDate")(self.replayDate),
           
            
            ])
    }

}
