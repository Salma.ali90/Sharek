//
//  TicketResponseData.swift
//  Sharek
//
//  Created by Salma on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import  Gloss

class AddTicketResponseData : NSObject, Glossy{
    
    
    var ticketId : Int
    
    required init?(json: JSON) {
    
        self.ticketId = Decoder.decode(key:"ticketId")(json)!
        
    }
    
    override init() {
        
        ticketId = 0
    }
    
    
    
    func toJSON() -> JSON? {
        return jsonify([
            
            Encoder.encode(key: "ticketId")(self.ticketId)
            
            ])
    }

}
