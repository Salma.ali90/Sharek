//
//  TicketImage.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/13/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import Gloss

class TicketImage: NSObject, Glossy {
    
    var imageUrl: String
    
    override init() {
        imageUrl = ""
    }
    
    init(imageUrl: String) {
        self.imageUrl = imageUrl
    }
    
    required init?(json : JSON){
        imageUrl = Decoder.decode(key: "imageUrl")(json)!
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            Encoder.encode(key: "imageUrl")(self.imageUrl)
            ])
    }
    

}
