//
//  AllTicketsData.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/13/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import Gloss

class AllTicketsData: NSObject,Glossy {
    
    var seenTickets : [Ticket]?
    var unSeenTickets : [Ticket]?
    
    
    override init() {
        
        seenTickets =  [Ticket]()
        unSeenTickets =  [Ticket]()
       
    }
    
    init(seenTickets: [Ticket],unSeenTickets:[Ticket]) {
        
        self.seenTickets = seenTickets
        self.unSeenTickets = unSeenTickets
    }
    
    required init?(json : JSON){
      
        seenTickets = Decoder.decode(decodableArrayForKey: "seenTickets")(json)
        unSeenTickets = Decoder.decode(decodableArrayForKey: "unSeenTickets")(json)
        
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            //  Encoder.encode(key: "Sponsors")(self.Sponsors),
            Encoder.encode(key: "seenTickets")(self.seenTickets),
            Encoder.encode(key: "unSeenTickets")(self.unSeenTickets)
            
            ])
    }
    

    
}
