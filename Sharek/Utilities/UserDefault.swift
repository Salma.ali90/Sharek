//
//  UserDefault.swift
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//
//

import Foundation
import UIKit

class UserDefault {
    
    static let USER_INFO = "user_info";
    static let USER_TOKEN = "user_token";
    static let LOCAL = "local";
    static let AutoSaving = "auto_save"
    static let IsUUID = "is_uuid"
    static let LocationEnabled = "location_enabled"
    static let LaunchedBefore = "LaunchedBefore"
    
    static let TIMEOUT_INTERVAL: TimeInterval = 100;
    
    let mUserDefault = UserDefaults.standard
    
    func setOneSignalID(_ token: String?) {
        mUserDefault.set(token, forKey: UserDefault.USER_TOKEN)
        mUserDefault.synchronize()
    }
    
    func getOneSignalID() -> String? {
        return mUserDefault.string(forKey: UserDefault.USER_TOKEN)
    }
    
    func setIsUUID(_ flag: Bool?) {
        mUserDefault.set(flag, forKey: UserDefault.IsUUID)
        mUserDefault.synchronize()
    }
    
    func isUUID() -> Bool? {
        return mUserDefault.bool(forKey: UserDefault.IsUUID)
    }
    
    func setLocal(_ local: String?) {
        mUserDefault.set(local, forKey: UserDefault.LOCAL)
        mUserDefault.synchronize()
    }
    
    func getLocal() -> String? {
        return mUserDefault.string(forKey: UserDefault.LOCAL)
    }
    
    
    func setAutoSaving(_ flag: Bool?) {
        mUserDefault.set(flag, forKey: UserDefault.AutoSaving)
        mUserDefault.synchronize()
    }
    
    func isAutoSaving() -> Bool? {
        return mUserDefault.bool(forKey: UserDefault.AutoSaving)
    }
    
    func setLocationStatus(_ flag: Bool?) {
        mUserDefault.set(flag, forKey: UserDefault.LocationEnabled)
        mUserDefault.synchronize()
    }
    
    func isLocationEnabled() -> Bool? {
        return mUserDefault.bool(forKey: UserDefault.LocationEnabled)
    }
    
    
    
    func setLaunchedBefore(_ token: Bool?) {
        
        mUserDefault.set(token, forKey: UserDefault.LaunchedBefore)
        mUserDefault.synchronize()
    }
    
    func isLaunchedBefore() -> Bool? {
        return mUserDefault.bool(forKey: UserDefault.LaunchedBefore)
    }

    
    func removeSession() {
        mUserDefault.removeObject(forKey: UserDefault.USER_TOKEN)
        mUserDefault.removeObject(forKey: UserDefault.USER_INFO)
        mUserDefault.synchronize()
    }
    
    
    
    
    
}







