//
//  ValidateUtil.swift
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//


import Foundation


class ValidateUtil {
    
    static func isValidateEmail(_ candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    static func isValidName(_ input:String) -> Bool {
        let stringRegex = "^[a-zA-Z0-9\\s\\u0600-\\u06ff\\u0750-\\u077f\\ufb50-\\ufc3f\\ufe70-\\ufefc]*";
            return NSPredicate(format: "SELF MATCHES %@", stringRegex).evaluate(with: input)
        
    }
    
   
    
    static func isValidPassword(_ input:String) -> Bool {
        let stringRegex = "^[a-zA-Z0-9\\s\\u0600-\\u06ff\\u0750-\\u077f\\ufb50-\\ufc3f\\ufe70-\\ufefc]*";
        

        if(NSPredicate(format: "SELF MATCHES %@", stringRegex).evaluate(with: input)){
            let stringcontainRegex = "(?=.*[a-zA-Z])(?=.*[0-9]).{8,}$";
            
            return NSPredicate(format: "SELF MATCHES %@", stringcontainRegex).evaluate(with: input)
            
        } else {
            return false
        }
       
        
    }
   

    static func isValidNumber(_ input:String) -> Bool {
        let stringRegex = "^[0-9]*$";
        return NSPredicate(format: "SELF MATCHES %@", stringRegex).evaluate(with: input)
    }

    static func isValidatePhoneNumber(_ number: String) -> Bool {
        let numberCharacters = NSCharacterSet.decimalDigits.inverted
        if !number.isEmpty && number.rangeOfCharacter(from: numberCharacters) == nil {
            return true
        }
        return false
    }
    
}
