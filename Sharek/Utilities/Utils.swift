//
//  Utils.swift
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//


import Foundation
import UIKit
import SystemConfiguration

class Utils {
   
    static func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }

        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    static func localizeNumber(_ num: Float) -> String{
        let userDefault = UserDefault()
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: userDefault.getLocal()!)
        return formatter.string(from: NSNumber(value: num))!
    }
    
    static func customFont(_ fontSize: CGFloat) -> UIFont{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(appDelegate.isRTL) {
          let   engfontSize = 18

            return UIFont(name: "Cairo-Light", size: CGFloat(engfontSize))!
        } else {
            let   arafontSize = 22

            return UIFont(name: "SansSerifFLF", size: CGFloat(arafontSize))!
        }
    }
    
    static func customDefaultFont(_ fontSize: CGFloat) -> UIFont{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(appDelegate.isRTL) {
            
            return UIFont(name: "Cairo-Light", size: fontSize)!
        } else {
            return UIFont(name: "SansSerifFLF", size: fontSize)!
        }
    }
    
    static func customDefaultLinkFont(_ fontSize: CGFloat) -> UIFont{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(appDelegate.isRTL) {
            
            return UIFont(name: "Cairo-Light", size: fontSize)!
        } else {
            let   engfontSize = 19
            return UIFont(name: "SansSerifFLF", size: CGFloat(engfontSize))!
        }
    }

    
    static func customBoldFont(_ fontSize: CGFloat) -> UIFont{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(appDelegate.isRTL) {
            let   engfontSize = 18

            return UIFont(name: "Cairo-Light", size: CGFloat(engfontSize))!
        } else {
            let   arafontSize =  22

            return UIFont(name: "SansSerifFLF", size: CGFloat(arafontSize))!
        }
    }
    
    static func localizeNumber(_ num: Int) -> String{
        let userDefault = UserDefault()
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: userDefault.getLocal()!)
        formatter.numberStyle = .none
        formatter.maximumFractionDigits = 2
        return formatter.string(from: NSNumber(value: num))!
    }

    
}
