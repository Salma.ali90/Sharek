//
//  AuthUtils.swift
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation


class AuthUtils {
    
    static let UN_AUTHORISED_CODE = 401
    static let EMPTY_RESPONSE_BODY_CODE = 504
    static let FORGET_PASSWORD_ERROR = 422
    static let SUCCESS_CODE = 200
    static let SERVER_ERROR = 500

    class func base64(_ input: String) -> String? {
        let plainData = input.data(using: String.Encoding.utf8)
        let base64String = plainData?.base64EncodedString(options: [])
        
        return base64String
    }
    
    class func nonce() -> (plainNonce: String, base64Nonce: String) {
        let temp = UUID().uuidString
        let plain = temp.replacingOccurrences(of: "-", with: "")
        let base64 = AuthUtils.base64(plain)!
        return (plainNonce: plain, base64Nonce: base64)
    }

    
    class func generateUserAuthRequest(_ userName: String, password: String) -> String {
        let request = NSMutableURLRequest()
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let nonce = AuthUtils.nonce()
        let headerValue = "Email=\"\(userName)\", Password=\"\(password)\", Nonce=\"\(nonce.base64Nonce)\""
        return headerValue
    }
    
}

