//
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//


import Foundation

protocol  Presenter  {
    func didLoad()
    func didAppear()
}
