//
//  Injection.swift
//  ThreeAges
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation

class Injection {
    
    static func  provideNetworkTickettRepository() -> TicketRepository {
        return NetworkTicketRepository()
    }

    //collection image in home
    //fb share url
    //push notifications
}
