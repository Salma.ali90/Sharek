//
//  AppDelegate.swift
//  Sharek
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit
import SideMenuController
import Fabric
import Crashlytics
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, OSSubscriptionObserver, OSPermissionObserver {

    var window: UIWindow?
    var isRTL: Bool = false
    var myDirection : NSTextAlignment?
    var images = [UIImage]()
    var counter = 0
    var textValue = String()
    var userDefault = UserDefault()
    var imagesId = [Int]()
    var isNeedingUpload = true
    var uuidString : String?

   

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        initLanguage()
       // loadAndSetRootWindow()
        let launchedBefore = userDefault.isLaunchedBefore()
        if launchedBefore!  {
            print("Not first launch.")
        } else {
            print("First launch, setting UserDefault.")
            userDefault.setAutoSaving(false)
            userDefault.setLaunchedBefore(true)
            userDefault.setLocationStatus(true)
        }
        Fabric.with([Crashlytics.self])
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: { (granted, error) in
//                    // Enable or disable features based on authorization
//                    if granted{
//                        print("auth granted")
//                        //self.userDefault.setNotificationsEnabled(true)
//                    }else{
//                        print("auth not granted")
//                       // self.userDefault.setNotificationsEnabled(false)
//                    }
//            })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "d87bd142-5fdb-49e9-8777-b395b288e955",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        OneSignal.add(self as OSPermissionObserver)
        OneSignal.add(self as OSSubscriptionObserver)
        
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        let userID = status.subscriptionStatus.userId
        print("userID = \(String(describing: userID))")
        let pushToken = status.subscriptionStatus.pushToken
        print("pushToken = \(String(describing: pushToken))")
        
        return true
    }

    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            // get player ID
            let userID = stateChanges.to.userId
            print("onOSSubscriptionChanged userID = \(String(describing: userID!))")
            
        }
        print("SubscriptionStateChange: \n\(stateChanges)")
        if  stateChanges.to.userId != nil {
            let userID = stateChanges.to.userId
            userDefault.setOneSignalID(userID!)
            userDefault.setIsUUID(false)
        }else{
            userDefault.setOneSignalID(getUUID())
            userDefault.setIsUUID(true)
        }
    }
    
    func onOSPermissionChanged(_ stateChanges: OSPermissionStateChanges!) {
        // Example of detecting answering the permission prompt
        if stateChanges.from.status == OSNotificationPermission.notDetermined {
            if stateChanges.to.status == OSNotificationPermission.authorized {
                //print("Thanks for accepting notifications!")
            } else if stateChanges.to.status == OSNotificationPermission.denied {
                print("Notifications not accepted. You can turn them on later under your iOS settings.")
            }
        }
        
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void){
        //print("didReceiveRemoteNotification: \(userInfo)")
        
        completionHandler(.newData)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        let notificationInfo = notification.request.content.userInfo as? NSDictionary
        print("willPresent \(String(describing: notificationInfo))")
        
        if let aps = notificationInfo!["custom"] as? NSDictionary {
            
            
            completionHandler([.alert, .badge, .sound])
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //Handle notification tap
        //print("didReceive \(response.description)")

        let notificationInfo = response.notification.request.content.userInfo as NSDictionary
        print("notificationInfo ",notificationInfo)
        if let aps = notificationInfo["custom"] as? NSDictionary {
            if let a = aps["a"] as? NSDictionary {
                let ticket = Ticket()
                
                ticket.ticketId  = a["ticketId"]! as? Int
                
                ticket.lat = a["lat"]! as? String
                ticket.lang = a["lang"]! as? String
                ticket.ticketEmotionId = a["ticketEmotionId"]! as? Int
                ticket.relatedTickestNo = a["relatedTickestNo"]! as? Int
                ticket.Location = a["location"]! as? String
                ticket.desc = a["description"]! as? String
                ticket.imagesUrl = a["imagesUrl"]! as? [String]
                let ticketReplyAr = a["replay"] as? [NSDictionary]
                if ticketReplyAr!.count > 0{
                for ticketReply in ticketReplyAr!{
                    let newTicketReply = TicketReply()
                    newTicketReply.replay = ticketReply["replay"]! as? String
                    let newTicketReplyDate = ticketReply["replayDate"]! as? NSDictionary
                    newTicketReply.replayDate = newTicketReplyDate!["date"]! as? String
                    ticket.replay?.append(newTicketReply)
                }
                }
                let ticketDate = a["ticketDate"]! as? NSDictionary
                ticket.ticketDate = ticketDate!["date"]! as? String
                
                let storyboard = UIStoryboard(name: "TicketDetails", bundle: nil)
                                       let destinationNavigationController = storyboard.instantiateViewController(withIdentifier: "detailsNavigation") as! UINavigationController
                                       let targetController = destinationNavigationController.topViewController as! TicketDetailsVC //detailsNavigation
                                    targetController.ticket = ticket
                                    targetController.isFromNotification = true
                                     self.window?.rootViewController = destinationNavigationController
            }
        }
        
        
        completionHandler()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

extension AppDelegate {
    
    func getUUID() -> String {
        uuidString = UIDevice.current.identifierForVendor?.uuidString
        NSLog("uuid is : %@", uuidString! as String)
        return uuidString!
    }
    func initLanguage() {
        //let pre = Locale.current.languageCode
        let languages = UserDefaults.standard.object(forKey: "AppleLanguages") as! [String]
        if(languages[0].contains("ar")) {
            isRTL = true
            userDefault.setLocal("ar")
            
        } else {
            isRTL = false
            userDefault.setLocal("en")
            
        }
        
        if(isRTL) {
            UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            UINavigationBar.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            UISearchBar.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            SideMenuController.preferences.drawing.sidePanelPosition = .underCenterPanelRight
            UITextView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            UITextField.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            
        } else {
            UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
            UINavigationBar.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
            UISearchBar.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
            SideMenuController.preferences.drawing.sidePanelPosition = .underCenterPanelLeft
            UITextView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
            UITextField.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
            
            
        }
        
        
        SideMenuController.preferences.drawing.sidePanelWidth = 250
        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.animating.transitionAnimator = nil
    }
    
    func hintRTL(textfild : UITextField)  {
        if isRTL {
            myDirection = NSTextAlignment.right
        }else{
            myDirection = NSTextAlignment.left
        }
        textfild.textAlignment = ((textfild.text?.characters.count)!==0) ? myDirection! : NSTextAlignment.natural;
    }
    
    func loadAndSetRootWindow() {
        
        autoreleasepool {
            
            var prevRootVC = self.window?.rootViewController
            var initialVC = UIViewController()
            let mainStoryboard  = UIStoryboard(name: "Home", bundle: nil)
                
                initialVC = mainStoryboard.instantiateViewController(withIdentifier: "navigationVC") as! NavigationVC
            
            self.window?.rootViewController = initialVC
            
            if(prevRootVC != nil) {
                prevRootVC?.dismiss(animated: false, completion: nil)
                prevRootVC = nil
            }
            
            self.window?.makeKeyAndVisible()
        }
    }

    
    func setLangauge(_ language: String) {
        var savedLanguages = UserDefaults.standard.object(forKey: "AppleLanguages") as! [String]
        savedLanguages[0] = language
        
        UserDefaults.standard.set(savedLanguages, forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        
        initLanguage()
    }
    
    
}
