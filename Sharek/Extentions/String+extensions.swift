//
//  String+extensions.swift
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit
import Foundation

extension String {
    
    func sha512() -> String? {
//        let passwordData = self.data(using: String.Encoding.utf8)
//        let len = Int(CC_SHA512_DIGEST_LENGTH)
//        let hash = UnsafeMutablePointer<UInt8>.allocate(capacity: len)
//        CC_SHA512((passwordData! as NSData).bytes, CC_LONG(passwordData!.count), hash);
//        let allData = NSMutableData()
//        allData.append(hash, length: len)
//        
//        for _ in 1 ..< 5000 {
//            allData.append((passwordData! as NSData).bytes, length:passwordData!.count);
//            let hashLoop = UnsafeMutablePointer<UInt8>.allocate(capacity: len)
//            CC_SHA512(allData.bytes, CC_LONG(allData.length), hashLoop);
//            allData.length = 0
//            allData.append(hashLoop,length:len)
       // }
        
     //   let imageData = Data(bytes: UnsafeRawPointer(allData.bytes), count: allData.length)
       // return imageData.base64EncodedString(options: NSData.Base64EncodingOptions())
        return ""
    }

    func localized() -> String {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let language : String = appDelegate.isRTL ? "ar":"Base"
        let path = Bundle.main.path(forResource: language, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    
}
