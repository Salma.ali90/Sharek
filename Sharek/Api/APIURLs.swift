//
//  APIURLs.swift
//  ThreeAges
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation


class APIURLs {
   
   static var MAIN_URL = "http://88.80.184.99/sharek/web/api/"
        
    static var ADD_TICKET = "tickets/adds"
    static var UPLOAD_IMAGE = "images/uploads"
    static var DELETE_IMAGE = "deletes/images"
    static var GET_TICKETS = "tickets"
    static var MARK_AS_SEEN = "marks/reads"
    static var SEARCH = "searches"
    static var RELATED_TICKETS = "relateds/tickets"
    static var CHANGE_USERID = "changes/devicetokens"
}
