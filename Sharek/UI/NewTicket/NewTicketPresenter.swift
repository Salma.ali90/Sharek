//
//  NewTicketPresenter.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/9/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import UIKit

class NewTicketPresenter: Presenter {
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var view : NewTicketView?
    var ticketRespository: TicketRepository!
    var mUserDefault = UserDefault()
    
    init (repository: TicketRepository) {
        ticketRespository = repository
        
    }
    
    func setView(_ view: NewTicketView) {
        weak var weakView = view
        self.view = weakView
       
    }

    func uploadImage(userImage:UIImage)
    {
       
        
        view?.startImageLoading()
        //call alamofire request
        
        ticketRespository.uploadImage(imageFile: userImage) { (result, error) in
            
            self.view?.finishImageLoading()
            if(error == NetworkTicketRepository.ErrorType.none) {
                
                    print(result)
                   // UPLOAD SUCCESSSFULLY
                   print("image uploaded")
                  self.view?.setImageId(result: result)
            }else {
                print("error")
            }
        }
    
    
    }
    
    func addTicket(ticket : Ticket , imagesIds :[Int])
    {
        view?.showloading()
       
        var images = [String:Int]()
        for i in 0..<imagesIds.count {
            images[String(i)] = imagesIds[i]
            
        }
        
        if(validateInputs(ticket: ticket, images: images)){
       ticketRespository.addTicket(lat: ticket.lat!, lang: ticket.lang!, Location: ticket.Location!, imagesUrl: images, desc: ticket.desc!, ticketEmotionId: ticket.ticketEmotionId!, deviceToken: mUserDefault.getOneSignalID()!) { (result, error) in //appDelegate.getUUID()
        
        self.view?.hideLoading()
        if(error == NetworkTicketRepository.ErrorType.none) {
            
            print("ticketID",result)
            self.view?.showSuccess(ticketNo: result)
        }else if(error == NetworkTicketRepository.ErrorType.network)
            {
                self.view?.showNetworkError()
        }else {
            self.view?.showServerError()
            print("error")
        }
        }
        }else{
            
            self.view?.hideLoading()
        self.view?.showInvlaidateToast("emptyMsg".localized())
        }
    }
    
    func validateInputs(ticket : Ticket , images : [String:Int]) -> Bool
    {
        if(!images.isEmpty && ticket.desc != "" && ticket.desc != "tellUs".localized() && ticket.ticketEmotionId != -2 )
        {
            return true
        }else{
            return false
        }
        
    }
    func didLoad(){}
    func didAppear(){}

    
}

protocol NewTicketView : class{
    
    func finishImageLoading()
    func startImageLoading()
    func setImageId(result : Int)
    func showloading()
    func hideLoading()
    func showNetworkError()
    func showServerError()
    func showSuccess(ticketNo : Int)
    func showInvlaidateToast(_ msg: String)
}
