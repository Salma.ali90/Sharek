//
//  NewTicketVC.swift
//  Sharek
//
//  Created by Salma on 7/29/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit
import CoreLocation
import Toaster
import MBProgressHUD
import Social

class NewTicketVC: UITableViewController , NewTicketView , UIActionSheetDelegate{


    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var textValue: UITextView!
    
    @IBOutlet weak var happyView: UIView!
    @IBOutlet weak var normalView: UIView!
    @IBOutlet weak var sadView: UIView!
    @IBOutlet weak var sendBarButton: UIBarButtonItem!
    
    @IBOutlet weak var editImageView: UIImageView!
    @IBOutlet weak var addPhotosLabel: UILabel!
    @IBOutlet weak var addedLabel: UILabel!
   
    var ticket = Ticket()
    var newImage : UIImage?
    var logoImages = [UIImage]()
    var isAvailable = true
    var appdelegate :AppDelegate?
    var userDefault = UserDefault()
    var presenter : NewTicketPresenter!
    var indexSelected : Int = 0
    var height = 0
    var locationManager:CLLocationManager!
    var loading: MBProgressHUD!
    var isFinishupLoading = false
    var isActionSheet = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorColor = UIColor.clear

        
        
        presenter = NewTicketPresenter(repository: Injection.provideNetworkTickettRepository())
        presenter.setView(self)
    
        appdelegate = UIApplication.shared.delegate as? AppDelegate
        //add it to app delegate array in app delgate and add counter
        if(newImage != nil){
        appdelegate?.images.append(newImage!)
        appdelegate?.counter += 1
        }
        self.tableView.tableFooterView = UIView()

      
       
        textValue.delegate=self
        textValue.text = "tellUs".localized()
        textValue.textColor = UIColor.lightGray
        intialization()
        
        if(userDefault.isLocationEnabled())!{
            determineMyCurrentLocation()
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
        if isActionSheet {
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
            return
        }
        logoImages = (appdelegate?.images)!
        if(logoImages.count < 8)
        {
            isAvailable = true
            logoImages.append(UIImage(named: "add_img")!)
        }else{
            isAvailable = false
            
        }

        if(appdelegate?.isNeedingUpload)!{
            newImage = appdelegate?.images[(appdelegate?.images.count)!-1]
            presenter.uploadImage(userImage: newImage!)
        }
        
        collectionView.reloadData()
        

        if (appdelegate?.isRTL)!{
            textValue.textAlignment = .right
        }else{
            textValue.textAlignment = .left

        }
        if appdelegate?.textValue == "tellUs"  || (appdelegate?.textValue.isEmpty)!{
            textValue.textColor = UIColor.lightGray
            textValue.text = "tellUs".localized()
           editImageView.image = UIImage(named: "write_ic")
        }else{
            textValue.text = appdelegate?.textValue
            textValue.textColor = UIColor.black
           editImageView.image = UIImage(named: "write_active_ic")
        }
         addedLabel.text = " \("added".localized()) (\(appdelegate?.counter ?? 0))"
       
       
    }
    
    func intialization()
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.happyViewPressed(_:)))
        
        happyView.addGestureRecognizer(tap)
        
        happyView.isUserInteractionEnabled = true

        let normaltap = UITapGestureRecognizer(target: self, action: #selector(self.normalViewPressed(_:)))
        
        normalView.addGestureRecognizer(normaltap)
        
        normalView.isUserInteractionEnabled = true
        
        let sadTap = UITapGestureRecognizer(target: self, action: #selector(self.sadViewPressed(_:)))
        
        sadView.addGestureRecognizer(sadTap)
        
        sadView.isUserInteractionEnabled = true
    
        
        happyView.layer.cornerRadius = 15
        happyView.clipsToBounds = true
        
        normalView.layer.cornerRadius = 15
        normalView.clipsToBounds = true
        
        sadView.layer.cornerRadius = 15
        sadView.clipsToBounds = true
        
        sendBarButton.title = "sendTicket".localized()
        addPhotosLabel.text = "addPhotos".localized()
        
        let tabGesture = UITapGestureRecognizer(target: self,action: #selector(dismissKeyboared))
        tabGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tabGesture)
        
    }
    
    
    @objc func happyViewPressed(_ sender: UITapGestureRecognizer) {
        ticket.ticketEmotionId = 1
        happyView.backgroundColor = UIColor.lightGray
        sadView.backgroundColor = UIColor.white
        normalView.backgroundColor = UIColor.white

    }
    
    @objc func normalViewPressed(_ sender: UITapGestureRecognizer) {
        ticket.ticketEmotionId = 0
        normalView.backgroundColor = UIColor.lightGray
        sadView.backgroundColor = UIColor.white
        happyView.backgroundColor = UIColor.white
    }
    
    @objc func sadViewPressed(_ sender: UITapGestureRecognizer) {
        ticket.ticketEmotionId = -1
        sadView.backgroundColor = UIColor.lightGray
        happyView.backgroundColor = UIColor.white
        normalView.backgroundColor = UIColor.white
    }
    @objc func dismissKeyboared() {
        view.endEditing(true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if indexPath.row == 0 {
        return 97
        }else if indexPath.row == 2{
            return CGFloat(height)
        }else if (indexPath.row == 1)
        {
            return 50 
        }
        return 250
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func happyPressed(_ sender: Any) {
        
        ticket.ticketEmotionId = 1
        happyView.backgroundColor = UIColor.lightGray
        sadView.backgroundColor = UIColor.white
        normalView.backgroundColor = UIColor.white

    }
    @IBAction func normalPressed(_ sender: Any) {
        ticket.ticketEmotionId = 0
        normalView.backgroundColor = UIColor.lightGray
        sadView.backgroundColor = UIColor.white
        happyView.backgroundColor = UIColor.white
    }
    @IBAction func sadPressed(_ sender: Any) {
        
        ticket.ticketEmotionId = -1
        sadView.backgroundColor = UIColor.lightGray
        happyView.backgroundColor = UIColor.white
        normalView.backgroundColor = UIColor.white
    }
    
    
    @IBAction func sendPressed(_ sender: Any) {
        
        ticket.desc = textValue.text
        dismissKeyboared()
        presenter.addTicket(ticket: ticket, imagesIds: (appdelegate?.imagesId)!)
        

    }
    
    

    @IBAction func cancelPressed(_ sender: Any) {
        
        appdelegate?.images.removeAll()
        appdelegate?.counter = 0
        appdelegate?.imagesId.removeAll()
        appdelegate?.textValue = "tellUs"
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }else{
            let alert = UIAlertController(title: "locationAlertTitle".localized(),
                                          message: "locationAlertMsg".localized(),
                                          preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "sendToSetting".localized(), style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) in
                print("GO TO SETTING")
                UIApplication.shared.openURL(NSURL(string: "App-Prefs:root=Privacy&path=LOCATION")! as URL)
            }))
            
            let cancelAction = UIAlertAction(title: "cancel".localized(),
                                             style: .cancel, handler: nil)
            
            alert.addAction(cancelAction)
            self.present(alert, animated: true)
            
        }
    }
    
    func getLocation(lat : Double , long : Double)
    {
    
       let location =  CLLocation(latitude: lat, longitude: long)
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, completionHandler: {
            placemarks, error in
            
            if error == nil && (placemarks?.count)! > 0 {
               let placeMark = placemarks?.last
                let lines  = "\(placeMark!.thoroughfare ?? "")\n\(placeMark!.postalCode ?? "") \(placeMark!.locality ?? "")\n\(placeMark!.administrativeArea ?? "")\n\(placeMark!.country ?? "")"
                
                let address = " \(placeMark!.locality ?? "") \(placeMark!.administrativeArea ?? "") \(placeMark!.country ?? "")"
                print(address)
                self.ticket.Location = address
                print(lines)
            }
        })
    }
    
    func startImageLoading(){
    
        logoImages.removeAll()
        logoImages = (appdelegate?.images)!
        logoImages.remove(at: logoImages.count-1)
        logoImages.append(UIImage(named: "loader")!)
        if(logoImages.count < 8)
        {
            isAvailable = true
            logoImages.append(UIImage(named: "add_img")!)
        }else{
            isAvailable = false
            
        }
        
        collectionView.reloadData()
    }
    
    func finishImageLoading()
    {
        
        logoImages = (appdelegate?.images)!
        if(logoImages.count < 8)
        {
            isAvailable = true
            logoImages.append(UIImage(named: "add_img")!)
        }else{
            isAvailable = false
            
        }
        
        collectionView.reloadData()
    }
    
    func setImageId(result : Int)
    {
        isFinishupLoading = true
        appdelegate?.imagesId.append(result)
    }
    
    func showloading()
    {
        isFinishupLoading = false
        loading = MBProgressHUD.showAdded(to: self.view, animated: true)
        loading.mode = MBProgressHUDMode.indeterminate
    }
    
    func hideLoading()
    {
        
        if(loading != nil) {
            loading.hide(animated: true)
            loading = nil
        }
    }
    func showNetworkError(){
    
        Toast(text: "connectionFailed".localized(), duration: Delay.short).show()
    
    }
    func showInvlaidateToast(_ msg: String){
         Toast(text: msg, duration: Delay.short).show()
    }
    func showServerError(){
        Toast(text: "error".localized(), duration: Delay.short).show()

    }
    func showSuccess(ticketNo: Int){
        
        appdelegate?.images.removeAll()
        appdelegate?.isNeedingUpload = false
        appdelegate?.counter = 0
        appdelegate?.imagesId.removeAll()
        
        let alert = UIAlertController(title: "Submission Number", message: "Ticket number is " + String(ticketNo) + " keep this number to check your submission anonymously", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Export", style: .default, handler: { (action: UIAlertAction!) in
            self.sharePressed(ticketNo: ticketNo)
        }))
        
        alert.addAction(UIAlertAction(title: "Done", style: .cancel, handler: { (action: UIAlertAction!) in
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "addImage"
        {
            let destinationController = segue.destination as! CameraGalleryVC
            destinationController.previousView=1
        }else if segue.identifier == "ShowImage"
        {
            let destinationController = segue.destination as! ImageDetailsVC

            destinationController.index = indexSelected
        }
    }

    @objc func sharePressed(ticketNo: Int)
    {
        
        let txt = "This is your ticket no " + String(ticketNo)
        let activityViewController = UIActivityViewController(activityItems: [txt], applicationActivities: nil)
        activityViewController.excludedActivityTypes = [
            UIActivityType.print,
            UIActivityType.saveToCameraRoll,
            UIActivityType.openInIBooks,
            UIActivityType.postToFacebook,
            UIActivityType.postToTwitter
        ]
        isActionSheet = true

        self.present(activityViewController, animated: true, completion: nil)
        activityViewController.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
            if !completed {
                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                
            }
        }

    }
}
extension NewTicketVC : UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return logoImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as!DetailsCollectionCell
        cell.ticketImageView.layer.cornerRadius = 5
        cell.ticketImageView.clipsToBounds = true
        cell.ticketImageView.image = logoImages[indexPath.row]
        height = Int(collectionView.contentSize.height)
        tableView.reloadData()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // let imageSelected = logoImages[indexPath.row]
        indexSelected = indexPath.row
        
        appdelegate?.textValue = self.textValue.text
        if(isAvailable)
        {
            if(indexPath.row==logoImages.count-1)
            {
                //open camera gallery
                
                performSegue(withIdentifier: "addImage", sender: self)
                
            }else{
                
                if(isFinishupLoading){
                print("image kbera")
                appdelegate?.isNeedingUpload = false
                //open whole image
                performSegue(withIdentifier: "ShowImage", sender: self)
                }
            }
        }else{
            
            if(isFinishupLoading){
            appdelegate?.isNeedingUpload = false
            print("image kbera")
            //show whole image
            performSegue(withIdentifier: "ShowImage", sender: self)
            }
        }
    }
    
}

extension NewTicketVC : CLLocationManagerDelegate{

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .notDetermined:
            manager.requestAlwaysAuthorization()
            break
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            manager.startUpdatingLocation()
            break
        case .restricted:
            // restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            Toast(text: "permissionDenied".localized(), duration: Delay.short).show()
            // user denied your app access to Location Services, but can grant access from Settings.app
            break
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation:CLLocation = locations[0] as CLLocation
        ticket.lat=userLocation.coordinate.latitude.description
        ticket.lang=userLocation.coordinate.longitude.description
        getLocation(lat: userLocation.coordinate.latitude, long: userLocation.coordinate.longitude)
        manager.stopUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
}

extension NewTicketVC : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            editImageView.image = UIImage(named: "write_active_ic")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "tellUs".localized()
            textView.textColor = UIColor.lightGray
        }
    }
    
   }
