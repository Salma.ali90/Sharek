//
//  HomeVC.swift
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit
import MBProgressHUD
import Toaster

class HomeVC: UIViewController,HomeView {

    @IBOutlet weak var noConnectionView: UIView!
    @IBOutlet weak var noConImage: UIImageView!
    @IBOutlet weak var lblNoConView: UILabel!
    @IBOutlet weak var tableView: UITableView!

    var homePresenter :HomePresenter!
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var timer = Timer()
    var loading: MBProgressHUD!
    var userDefault: UserDefault!
    var seenTickets = [Ticket]()
    var unSeenTickets = [Ticket]()
    var selectedTicket = Ticket()
    var count = 0
    var imagesCount = [Int]()
    var allTickets = [Ticket]()
    var ticket = Ticket()

    override func viewDidLoad() {
        super.viewDidLoad()

        userDefault = UserDefault()
        initView();
    }
    
//    deinit {
//        NotificationCenter.default.removeObserver(self, name: Notification.Name("NotificationIdentifier"), object: nil)
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        homePresenter = HomePresenter(repository: Injection.provideNetworkTickettRepository())
        homePresenter.setView(self)
    }
 
    
    
    func initView(){
        
        navigationController?.navigationBar.topItem?.title = "sharek".localized()
       // let tryGesture = UITapGestureRecognizer(target: self, action: #selector(tryAgain))
       // let tryimageGesture = UITapGestureRecognizer(target: self, action: #selector(tryAgain))
       // lblNoConView.addGestureRecognizer(tryGesture)
       // noConImage.addGestureRecognizer(tryimageGesture)
    }

    func tryAgain() {
        homePresenter.tryAgain()
    }
    
    //view

    func showloading() {
        loading = MBProgressHUD.showAdded(to: self.view, animated: true)
        loading.mode = MBProgressHUDMode.indeterminate
    }
    
    func hideLoading() {

        if(loading != nil) {
            loading.hide(animated: true)
            loading = nil
        }
    }
    
    func showNoData(){
   
        Toast(text: "no_data".localized(), duration: Delay.short).show()

    }
    
    func showNetworkError(){
        Toast(text: "connectionFailed".localized(), duration: Delay.short).show()
        

    }
    
    
    func setDate(allTickets : AllTicketsData ){
        seenTickets=allTickets.seenTickets!
        unSeenTickets=allTickets.unSeenTickets!
        self.allTickets.append(contentsOf: unSeenTickets)
        self.allTickets.append(contentsOf: seenTickets)
        self.tableView.reloadData()
    }
    
    

    @IBAction func menuAction(_ sender: Any) {
        sideMenuController?.toggle()
    }
    
    @IBAction func searchAction(_ sender: Any) {
        performSegue(withIdentifier: "searchSegue", sender: self)

    }
    
    func showSuccessdialoug(){
        
    }
    
    func showServerError() {
       Toast(text: "error".localized(), duration: Delay.short).show()
    }
    
}

extension HomeVC : UITableViewDelegate, UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return unSeenTickets.count
        }
        return seenTickets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PostCell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostCell
        cell.selectionStyle = .none
        //tableViewCell =  tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostCell
       
        switch indexPath.section {
        case 0:
            ticket = unSeenTickets[indexPath.row]
            cell.ticketLabel.textColor = UIColor.blue
            break
        case 1:
           ticket = seenTickets[indexPath.row]
           
           //let lightColor = UIColor.init(red: 0.961, green: 0.957, blue: 0945, alpha: 1)

           cell.ticketLabel.textColor = UIColor.lightGray
           break
            
        default: break
           
        }
        
        if(ticket.ticketEmotionId == 0)
        {
            cell.emoImageView.image = UIImage(named: "shoked_ic.png")
        }else if(ticket.ticketEmotionId == 1)
        {
            cell.emoImageView.image = UIImage(named: "happy_ic.png")
        }else{
            cell.emoImageView.image = UIImage(named: "angry_ic.png")
        }
        
        cell.ticketLabel.text = ticket.desc
        
        if((ticket.replay?.count)! > 0)
        {
            cell.responseLabel.text = ticket.replay?[0].replay
        }else{
            
            cell.responseLabel.text = ""
        }
        
        
        let formatter = DateFormatter()
        formatter.calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.ISO8601)! as Calendar
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: TimeZone.current.secondsFromGMT()) as TimeZone!
        formatter.dateFormat = "hh:mm a"
        
        let dateFormatter = DateFormatter()
      //yyyy-MM-ddEHH:mm:ssZ //yyyy-MM-dd'T'HH:mm:ssZ
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //yyyy-MM-dd HH:mm:ss
        dateFormatter.timeZone = TimeZone(abbreviation: TimeZone.current.abbreviation()!) //Current time zone
        // print(ticket.ticketDate)
        let date = dateFormatter.date(from: ticket.ticketDate!) //according to date format your date string
        print(date ?? "")
        let dateString = formatter.string(from: date!)
        print(dateString)
        
        cell.dateLabel.text = dateString
        
//2018-01-01T11:43:34+00:00
       //cell.setCell(ticket)
        
        if ticket.imagesUrl?.count != nil {
            imagesCount.append(ticket.imagesUrl!.count)
            cell.collectionView.reloadData()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var id = 0
        if(indexPath.section == 0 )
        {
            id = unSeenTickets[indexPath.row].ticketId!
             homePresenter.markAsSeen(ticketId: id)
        }
       
        performSegue(withIdentifier: "detailsSegue", sender: self)
    }
    

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell: HeaderCell  = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! HeaderCell
        
            if section == 0 {
                
                let headerText = " \("newResponse".localized()) ( \(unSeenTickets.count) )"
                cell.sectionLabel.text = headerText
            }else{
                let headerText = " \("previousResponse".localized()) ( \(seenTickets.count) )"
                cell.sectionLabel.text = headerText
                //cell.sectionLabel.text = "previousResponse".localized()
            }
        
        
        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "cameraSegue")
        {
            let destinationController = segue.destination as! CameraGalleryVC
            destinationController.previousView=0
        } else if (segue.identifier == "detailsSegue"){
            var tableIndexPath = self.tableView.indexPathForSelectedRow
            let section = tableIndexPath?.section
            
            if(section == 0 ){
                selectedTicket = unSeenTickets[(tableIndexPath?.row)!]
            }else {
                selectedTicket = seenTickets[(tableIndexPath?.row)!]
                
            }

            let nav = segue.destination as! UINavigationController
            let detailsVC = nav.topViewController as! TicketDetailsVC
            detailsVC.ticket = self.selectedTicket
        }else if (segue.identifier == "searchSegue"){
            //let detailsVC = segue.destination as! SearchVC
            //detailsVC.tickets = self.allTickets
        }
    }
}

extension HomeVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(imagesCount.count > 0){
            var count = 0
            for i in 0..<imagesCount.count {
                
                count = imagesCount[i]
            }
            return count
        }
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PostCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "postImageCell", for: indexPath) as! PostCollectionCell
        let urlArr = ticket.imagesUrl!
        let url = URL(string: (urlArr[indexPath.row]))
        cell.ticketImageView.kf.setImage(with: url, placeholder: UIImage(named : "loader"), options: nil, progressBlock: nil, completionHandler: nil)
        //cell.setCell(ticket,indexPath: indexPath)
    
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
               //performSegue(withIdentifier: "detailsSegue", sender: self)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
}


