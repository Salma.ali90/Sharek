//
//  HomePresenter.swift
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import UIKit

class HomePresenter:  Presenter {
    
    var view : HomeView?
    var ticketRepository: TicketRepository!


    var mUserDefault = UserDefault()
    var appDelegate = UIApplication.shared.delegate as! AppDelegate

    
    init (repository: TicketRepository) {
        self.ticketRepository = repository

    }
    
    func setView(_ view: HomeView) {
        weak var weakView = view
        self.view = weakView
        getAllTickets()
        
    }
    
    func didLoad() {
    }
    
    func didAppear() {
    }
    
    func tryAgain() {
    

    }
    
    func updateUserId()
    {
        
      //  view?.showloading()
        //appDelegate.getUUID()

          ticketRepository.changeUserId(deviceToken: appDelegate.getUUID(), newDeviceToken: mUserDefault.getOneSignalID()!) { (result, error) in
            
           // self.view?.hideLoading()
            if(error == NetworkTicketRepository.ErrorType.none)
            {

            }else if error == NetworkTicketRepository.ErrorType.serverError
            {
                //self.view?.showServerError()
                print("server error")
            }else{
                
                self.view?.showNetworkError()
                print("network failure")
            }
        }
        
    }
    
    func getAllTickets()
    {
        
        view?.showloading()
        //appDelegate.getUUID()
        ticketRepository.getAllTickets(deviceToken: mUserDefault.getOneSignalID()!) { (result, error) in
        
            self.view?.hideLoading()
            if(error == NetworkTicketRepository.ErrorType.none)
            {
                if  self.mUserDefault.isUUID()! {
                    self.updateUserId()
                }
                if((result.seenTickets?.isEmpty)! && (result.unSeenTickets?.isEmpty)!)
                {
                    self.view?.showNoData()
                }else{
                    self.view?.setDate(allTickets: result)
                }
            }else if error == NetworkTicketRepository.ErrorType.serverError
            {
                self.view?.showServerError()
                print("server error")
            }else{
                
                self.view?.showNetworkError()
                print("network failure")
            }
        }
        
    }
    
    
    func markAsSeen(ticketId : Int)
    {
        
        ticketRepository.markAsSeen(deviceToken:mUserDefault.getOneSignalID()! , ticketId: ticketId) { (result, error) in //appDelegate.getUUID()
            
            if(error == NetworkTicketRepository.ErrorType.none) {
                
                print(result)
                print("Seen")
                
            }else if(error == NetworkTicketRepository.ErrorType.network)
            {
                self.view?.showNetworkError()
            }else {
                self.view?.showServerError()
                print("error")
            }

        }
    }
       
  }

protocol HomeView : class {
    func showloading()
    func hideLoading()
    func showNoData()
    func showNetworkError()
    func setDate(allTickets : AllTicketsData )
    func showServerError()

}
