//
//  SettingsVC.swift
//  ThreeAges
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit
import MBProgressHUD

class SettingsVC: UITableViewController,SettingsView {

    @IBOutlet weak var saveGalleryLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var locationSwitch: UIButton!
    @IBOutlet weak var locationHintLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var langPickerView: UIPickerView!
    @IBOutlet weak var gallerySwitch: UIButton!
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    //static var fromWhere = ""
    var userDefault: UserDefault!
    var selectedLanguage = ""
    var languagePresenter :SettingsPresenter!
    var loading: MBProgressHUD!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        userDefault = UserDefault()
        languagePresenter = SettingsPresenter(repository: Injection.provideNetworkTickettRepository())
        languagePresenter.setView(self)
        
        gallerySwitch.isSelected = userDefault.isAutoSaving()!
        gallerySwitch.setImage(UIImage(named: "Switch_on"), for: .selected)
        gallerySwitch.setImage(UIImage(named: "Switch_off"), for: .normal)
        locationSwitch.isSelected = userDefault.isLocationEnabled()!
        locationSwitch.setImage(UIImage(named: "Switch_on"), for: .selected)
        locationSwitch.setImage(UIImage(named: "Switch_off"), for: .normal)
        
        localizeStrings()
        //setUI()
    }
    
    @IBAction func galleryToggle(_ sender: Any) {
        
         gallerySwitch.isSelected = !gallerySwitch.isSelected
        if gallerySwitch.isSelected {
                        userDefault.setAutoSaving(true)
                    } else {
                         userDefault.setAutoSaving(false)
                    }
       
    }
    
    @IBAction func locationToggle(_ sender: Any) {
        
        locationSwitch.isSelected = !locationSwitch.isSelected
        if locationSwitch.isSelected {
            userDefault.setLocationStatus(true)
        } else {
            userDefault.setLocationStatus(false)
        }
        
    }
       func localizeStrings() {
        navigationController?.navigationBar.topItem?.title = "menuSettings".localized()
        let titleDict: NSDictionary = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = (titleDict as! [NSAttributedStringKey : Any])
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]

        saveGalleryLabel.text = "saveGallery".localized()
        locationLabel.text = "location".localized()
        locationHintLabel.text = "locationText".localized()
        languageLabel.text = "language".localized()
        //locationHintLabel.text = "".localized()
        
        if(appDelegate.isRTL) {
            selectedLanguage = "ar"
            
        } else {
            selectedLanguage = "en"
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        print("hello")
       // sideMenuController?.centerViewController.dismiss(animated: true, completion: nil)
       self.dismiss(animated: true, completion: nil)
       //self.navigationController?.popViewController(animated: true)
    }

    func toogleMenu(sender: UIBarButtonItem) {
        sideMenuController?.toggle()
    }
    
    
    @IBAction func changeLang(_ sender: Any) {
//         if(appDelegate.isGuest) {
//             setSuccessLanguage()
//         }else {
//            languagePresenter.changeLanguage(lang: selectedLanguage)
//        }
    }
    

    @IBAction func toggleMenu(_ sender: Any) {
        self.sideMenuController?.toggle()
    }
    
    func setUI() {
        let backButton = UIBarButtonItem(title: nil, style: .plain, target: self, action: #selector(backAction(_:)))
        if(appDelegate.isRTL) {
            backButton.image = UIImage(named: "back_ar_ic")
            
        } else {
            backButton.image = UIImage(named: "back_en_ic")
        }
        backButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItems = [backButton]
        
    

    }
    


    //view
    func showloading() {
        loading = MBProgressHUD.showAdded(to: self.view, animated: true)
        loading.mode = MBProgressHUDMode.indeterminate
    }
    
    func hideLoading() {
        if(loading != nil) {
            loading.hide(animated: true)
            loading = nil
        }
    }
    
    func showNetworkError() {
       // view.makeToast("connectionFailed".localized())
    }
    
    func setSuccessLanguage() {
        if(selectedLanguage == "ar") {
            appDelegate.isRTL = true
            appDelegate.setLangauge("ar")
            userDefault.setLocal("ar")
            
        } else {
            appDelegate.isRTL = false
            appDelegate.setLangauge("en")
            userDefault.setLocal("en")
        }
        appDelegate.loadAndSetRootWindow()

    }
    
    func showError(error:String) {
       // view.makeToast(error)
 
    }

}

extension SettingsVC : UIPickerViewDelegate,UIPickerViewDataSource{

    // DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
     return 3
    }
    
    // Delegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return "default"
        }
        else if row == 1 {
            return "ar".localized()
        }
        return "en".localized()
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 {

        }else if row == 1 {
                selectedLanguage = "ar"
        }else{
            selectedLanguage = "en"
        }
        setSuccessLanguage()
    }
}
