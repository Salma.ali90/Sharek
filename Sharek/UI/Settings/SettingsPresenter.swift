//
//  SettingsPresenter.swift
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import UIKit

class SettingsPresenter :Presenter {
    
    var view : SettingsView?
    var userRepository: TicketRepository!
    
    var mUserDefault = UserDefault()
    var appDelegate = UIApplication.shared.delegate as! AppDelegate

    
    init (repository: TicketRepository) {
        userRepository = repository
        
    }
    
    func setView(_ view: SettingsView) {
        weak var weakView = view
        self.view = weakView
    }
    
    func didLoad() {
    }
    
    func didAppear() {
    }
    
    
     func changeLanguage(lang:String) {

        self.view?.showloading()
        
//       userRepository.changeLanguage(self.mUserDefault.getLocal()!, token: mUserDefault.getToken()!,lang:lang) {(string, error) in
//            self.view?.hideLoading()
//            if(error == NetworkUserRepository.ErrorType.none) {
//                if(string == "error".localized()){
//                    self.view?.showError(error: "error".localized())
//                }else {
//                    self.view?.setSuccessLanguage()
//                }
//                
//            } else if(error == NetworkUserRepository.ErrorType.auth) {
//                self.view?.hideLoading()
//                 self.mUserDefault.setToken(nil)
//            self.appDelegate.loadAndSetRootWindow()
//                
//            } else if(error == NetworkUserRepository.ErrorType.network) {
//                self.view?.hideLoading()
//                self.view?.showNetworkError()
//            } else {
//                self.view?.showError(error: "error".localized())
//  
//        }
//        }
    }
    
}

protocol SettingsView : class {
    func showloading()
    func hideLoading()
    func showNetworkError()
    func setSuccessLanguage()
    func showError(error:String)
    
}
