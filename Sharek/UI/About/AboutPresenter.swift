//
//  AboutPresenter.swift
//  Sharek
//
//  Created by Salma on 7/29/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit

class AboutPresenter :Presenter {
    
    var view : AboutView?
    var userRepository: TicketRepository!
    
    //var mUserDefault = UserDefault()
    //var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    init (repository: TicketRepository) {
        userRepository = repository
        
    }
    
    func setView(_ view: AboutView) {
        weak var weakView = view
        self.view = weakView
    }
    
    func didLoad() {
    }
    
    func didAppear() {
    }
    
}

protocol AboutView : class {
    func showloading()
    func hideLoading()
    func showNetworkError()
    func setSuccessLanguage()
    func showError(error:String)
    
}
