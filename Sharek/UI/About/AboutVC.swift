//
//  AboutVC.swift
//  Sharek
//
//  Created by Salma on 7/29/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit

class AboutVC: UIViewController, AboutView {
    
    //static var fromWhere = ""
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func backAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }

    func setUI()
    {
        let titleDict: NSDictionary = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = (titleDict as! [NSAttributedStringKey : Any])
        navigationController?.navigationBar.topItem?.title = "menuAbout".localized()
        
//        let backButton = UIBarButtonItem(title: nil, style: .plain, target: self, action: #selector(backAction(_:)))
//        if(appDelegate.isRTL) {
//            backButton.image = UIImage(named: "back_ar_ic")
//            
//        } else {
//            backButton.image = UIImage(named: "back_en_ic")
//        }
//        backButton.tintColor = UIColor.white
//        navigationItem.leftBarButtonItems = [backButton]

    }
    
    @IBAction func toggleMenu(_ sender: Any) {
        self.sideMenuController?.toggle()
    }
    
    func showloading(){
    }
    
    func hideLoading(){
    
    }
    
    func showNetworkError(){
    
    }
    
    func setSuccessLanguage(){
    }
    
    func showError(error:String){
    }

}
