//
//  SplashVC.swift
//  Sharek
//
//  Created by Salma Ali on 8/20/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit
import SwiftGifOrigin

class SplashVC: UIViewController {
    
@IBOutlet weak var SplashGIF: UIImageView!
    
      var appDelegate: AppDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        let splashGifresource = UIImage.gif(name: "splash")
        
    
        SplashGIF.animationImages = splashGifresource?.images
        // Set the duration of the UIImage
        SplashGIF.animationDuration = splashGifresource!.duration
        // Set the repetitioncount
        SplashGIF.animationRepeatCount = 1
        // Start the animation
        SplashGIF.startAnimating()
        
        let when = DispatchTime.now() + splashGifresource!.duration // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            //self.performSegue(withIdentifier: "showHome", sender: self)
            
          self.startApp()
        }
        
    }
    
    func startApp() {
        appDelegate?.loadAndSetRootWindow()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
}
