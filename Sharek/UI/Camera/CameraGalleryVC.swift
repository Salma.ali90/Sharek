//
//  CameraGalleryVC.swift
//  Sharek
//
//  Created by Salma on 8/2/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit

class CameraGalleryVC: UIViewController ,UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    @IBOutlet weak var imageView: UIImageView!
   // @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var closeView: UIView!
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var photosNo: UIButton!
    
    
    
    
    let imagePicker = UIImagePickerController()
    var  image=UIImage()
    var userDefault: UserDefault!
    var previousView = 0
    var appdelegate:AppDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        intialization()
    }
    
    
    func intialization()
    {
        imagePicker.delegate = self
        appdelegate = UIApplication.shared.delegate as? AppDelegate
        userDefault = UserDefault()
        //saveButton.setTitle("save".localized(),for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        closeView.addGestureRecognizer(tap)
        
      closeView.isUserInteractionEnabled = true
        
        if(previousView == 1)
        {
            //the previous view is galleryCamera
            print("galleryCamera")
            let imageNumber = appdelegate?.counter
            let title = "photos".localized()
            
            
            if(appdelegate?.isRTL)! {
                closeButton.setImage(UIImage(named: "back_ar_ic"), for: .normal)
                
                
            } else {
                closeButton.setImage(UIImage(named: "back_en_ic"), for: .normal)
            }
            
            photosNo.isHidden = false
            photosNo.setTitle(" \(title)  \(imageNumber!)",for: .normal)
            
        }else{
            //the previous view is home
            print("home")
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func cameraPressed(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            print("Camera not available")
        }
        
    }
    
    
    @IBAction func galleryPressed(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        appdelegate?.isNeedingUpload = false
        self.dismiss(animated: true, completion: nil)
        
    }
    
//    @IBAction func savePressed(_ sender: Any) {
//         appdelegate?.isNeedingUpload = true
//        if(previousView == 0){ // 0 is home
//            performSegue(withIdentifier: "newTicket", sender: self)
//        }else{
//            appdelegate?.images.append(image)
//            print(appdelegate?.images.count ?? "")
//            appdelegate?.counter += 1
//            self.dismiss(animated: true, completion: nil)
//        }
//
//    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageView.image = image
        
        if(userDefault.isAutoSaving())!{
            UIImageWriteToSavedPhotosAlbum(self.image, self, nil, nil)
        }
        
        picker.dismiss(animated: true, completion: nil)
       // saveButton.isHidden = false
        
        appdelegate?.isNeedingUpload = true
        if(previousView == 0){ // 0 is home
            performSegue(withIdentifier: "newTicket", sender: self)
        }else{
            appdelegate?.images.append(image)
            print(appdelegate?.images.count ?? "")
            appdelegate?.counter += 1
            self.dismiss(animated: true, completion: nil)
        }

    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destinationNavigationController = segue.destination as! UINavigationController
        let targetController = destinationNavigationController.topViewController as! NewTicketVC
        targetController.newImage = image
        
        
    }
    
    
    
   
}
