//
//  SearchVC.swift
//  Sharek
//
//  Created by Salma on 8/19/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit
import MBProgressHUD
import Toaster

class SearchVC: UIViewController, SearchView {
    
    @IBOutlet var tableView: UITableView!

    var tickets = [Ticket]()
    var ticket = Ticket()
    var imagesCount = [Int]()
    var presenter :SearchPresenter!
    var loading: MBProgressHUD!
    var selectedTicket = Ticket()
    
    var filteredTickets = [Ticket]()
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = SearchPresenter(repository: Injection.provideNetworkTickettRepository())
        presenter.setView(self)

        
        //filteredTickets = tickets
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchBar.barTintColor = Colors.hexStringToUIColor(hex: "4285F4")
        searchController.searchBar.tintColor = .white

//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
//            statusBar.backgroundColor = Colors.hexStringToUIColor(hex: "4285F4")
//        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        searchController.isActive = true
        DispatchQueue.main.async {
            self.searchController.searchBar.becomeFirstResponder()
        }
    }

    func showloading() {
        loading = MBProgressHUD.showAdded(to: self.view, animated: true)
        loading.mode = MBProgressHUDMode.indeterminate
    }
    
    func hideLoading() {
        
        if(loading != nil) {
            loading.hide(animated: true)
            loading = nil
        }
    }
    
    
    func showNetworkError(){
        Toast(text: "connectionFailed".localized(), duration: Delay.short).show()
        
        
    }
    
    func showServerError() {
        Toast(text: "error".localized(), duration: Delay.short).show()
    }
    
    
    
    func setData(tickets : [Ticket]){
        
        self.tickets = tickets
        //filteredTickets = tickets
        tableView.reloadData()
    }
    
    func showNoData(){
        Toast(text: "no_data".localized(), duration: Delay.short).show()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "searchDetailsSegue"){
            var tableIndexPath = self.tableView.indexPathForSelectedRow
      
            selectedTicket = tickets[(tableIndexPath?.row)!]
            let nav = segue.destination as! UINavigationController
            let detailsVC = nav.topViewController as! TicketDetailsVC
            
            detailsVC.ticket = self.selectedTicket
        }
    }

}

extension SearchVC: UITableViewDelegate, UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return self.filteredTickets.count
        if self.tickets.count == 0 {
            return 0
        }
        return self.tickets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        let cell : PostCell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostCell
        cell.selectionStyle = .none
        //cell.ticketLabel.text = self.filteredTickets[indexPath.row].desc
        
        ticket = tickets[indexPath.row]
        cell.ticketLabel.textColor = UIColor.blue
        
        if(ticket.ticketEmotionId == 0)
        {
            cell.emoImageView.image = UIImage(named: "shoked_ic.png")
        }else if(ticket.ticketEmotionId == 1)
        {
            cell.emoImageView.image = UIImage(named: "happy_ic.png")
        }else{
            cell.emoImageView.image = UIImage(named: "angry_ic.png")
        }
        
        cell.ticketLabel.text = ticket.desc
        
        if((ticket.replay?.count)! > 0)
        {
            cell.responseLabel.text = ticket.replay?[0].replay
        }else{
            
            cell.responseLabel.text = ""
        }
        
        let formatter = DateFormatter()
        formatter.calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.ISO8601)! as Calendar
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
        formatter.dateFormat = "hh:mm a"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-ddEHH:mm:ssZ" //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        // print(ticket.ticketDate)
        let date = dateFormatter.date(from: ticket.ticketDate!) //according to date format your date string
        print(date ?? "")
        let dateString = formatter.string(from: date!)
        print(dateString)
        
        cell.dateLabel.text = dateString
        
        if ticket.imagesUrl?.count != nil {
            imagesCount.append(ticket.imagesUrl!.count)
            cell.collectionView.reloadData()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("search Row \(indexPath.row) selected")
        self.searchController.dismiss(animated: false) {
            self.performSegue(withIdentifier: "searchDetailsSegue", sender: self)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
        
    }
}

extension SearchVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        //        let number = self.allTickets[count].imagesUrl?.count
        //        count += 1
        //        return number!
        if(imagesCount.count > 0){
            var count = 0
            for i in 0..<imagesCount.count {
                
                count = imagesCount[i]
            }
            return count
        }
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PostCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "postImageCell", for: indexPath) as! PostCollectionCell
        let urlArr = ticket.imagesUrl!
        let url = URL(string: (urlArr[indexPath.row]))
        cell.ticketImageView.kf.setImage(with: url, placeholder: UIImage(named : "loader"), options: nil, progressBlock: nil, completionHandler: nil)
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //performSegue(withIdentifier: "detailsSegue", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
}


extension SearchVC: UISearchControllerDelegate,UISearchResultsUpdating, UISearchBarDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {
        // If we haven't typed anything into the search bar then do not filter the results
        if searchController.searchBar.text! == "" {
            filteredTickets = tickets
        } else {
            // Filter the results
            
            filteredTickets = tickets.filter { $0.desc!.lowercased().contains(searchController.searchBar.text!.lowercased()) }
        }
        
       // self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("Search keyboard")
        if searchBar.text != nil && searchBar.text! != " " && searchBar.text!.characters.count > 3{
            presenter.search(ticketIdPart: searchBar.text!)
        }else{
            Toast(text: "searchCount".localized(), duration: Delay.short).show()
        }
    }
}
