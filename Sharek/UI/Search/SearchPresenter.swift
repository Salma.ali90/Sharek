//
//  SearchPresenter.swift
//  Sharek
//
//  Created by Salma on 8/19/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit

class SearchPresenter:  Presenter {
    
    var view : SearchView?
    var ticketRepository: TicketRepository!
    
    
    var mUserDefault = UserDefault()
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    init (repository: TicketRepository) {
        self.ticketRepository = repository
        
    }
    
    func setView(_ view: SearchView) {
        weak var weakView = view
        self.view = weakView
        
    }
    
    func didLoad() {
    }
    
    func didAppear() {
    }
    
    func tryAgain() {
        
    }
    
    
    func search(ticketIdPart : String){
        //appDelegate.getUUID()
        ticketRepository.searchTicketNo(deviceToken: mUserDefault.getOneSignalID()!, ticketIdPart: ticketIdPart) { (result, error) in
            self.view?.hideLoading()
            if(error == NetworkTicketRepository.ErrorType.none){
                if(result.isEmpty){
                    self.view?.showNoData()
                }else{
                    self.view?.setData(tickets: result)
                }
            }else if error == NetworkTicketRepository.ErrorType.serverError{
                self.view?.showServerError()
                print("server error")
            }else{
                self.view?.showNetworkError()
                print("network failure")
            }
        }
        
    }
    
}

protocol SearchView : class {
    func showloading()
    func hideLoading()
    func showNoData()
    func showNetworkError()
    func setData(tickets : [Ticket])
    func showServerError()
}
