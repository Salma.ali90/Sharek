//
//  TicketDetailsPresenter.swift
//  Sharek
//
//  Created by Salma on 7/29/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit

class TicketDetailsPresenter:  Presenter {
    
    var view : DetailsView?
    var eventRepository: TicketRepository!
    var userRepository: TicketRepository!
    
    var mUserDefault = UserDefault()
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    init (repository: TicketRepository,userRepository:TicketRepository) {
        eventRepository = repository
        self.userRepository = userRepository
        
    }
    
    func setView(_ view: DetailsView) {
        weak var weakView = view
        self.view = weakView
    
    }
    
    func didLoad() {
    }
    
    func didAppear() {
    }
    
    func tryAgain() {
        
        
    }
    
       
}

protocol DetailsView : class {
    func showloading()
    func showloadingEvent()
    func hideLoading()
    func showNoData()
    func showNetworkError()
    func setDate()
    func setEvent(post:Ticket)
    func showServerError()
    
}
