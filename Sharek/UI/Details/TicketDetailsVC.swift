//
//  TicketDetailsVC.swift
//  Sharek
//
//  Created by Salma on 7/29/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit
import Social


class TicketDetailsVC: UITableViewController,RelatedTicketsProtocol {

    var ticket = Ticket()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var item = 0
    var isFromNotification = false

    override func viewDidLoad() {
        super.viewDidLoad()
        intialization()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
       // return (ticket.replay?.count)!
        if (ticket.replay?.count)! > 0 {
            return 1
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
          let  cell: DetailsInfoCell = tableView.dequeueReusableCell(withIdentifier: "detailsInfoCell", for: indexPath) as! DetailsInfoCell
                cell.selectionStyle = .none
            
          cell.setCell(ticket: ticket, isFromNotification: isFromNotification)
            return cell
        } else {
           let cell: DetailsCommentCell = tableView.dequeueReusableCell(withIdentifier: "detailsICommentCell", for: indexPath) as! DetailsCommentCell
            cell.selectionStyle = .none
            cell.setCell(ticket: ticket, indexPath: indexPath, relatedTicketsDelegete: self ,isFromNotification :isFromNotification)
            
//            let tap = UITapGestureRecognizer(target: self, action: #selector(self.getRelatedTickets(_:)))
//            
//            cell.responseImageView.addGestureRecognizer(tap)
//            
//            cell.responseImageView.isUserInteractionEnabled = true
            return cell
        }
    }
    
    
        override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
                return 205
        }
        return 100
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // performSegue(withIdentifier: "detailsSegue", sender: self)
    }
    
    
    func intialization()
    {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage(named: "share.png"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(self.sharePressed), for: UIControlEvents.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 20, height: 51)
        let barButton = UIBarButtonItem(customView: button)
       

        
        let backButton = UIBarButtonItem(title: nil, style: .plain, target: self, action: #selector(backAction(_:)))
         backButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = barButton
        self.navigationItem.leftBarButtonItems = [backButton]
        if(appDelegate.isRTL) {
            backButton.image = UIImage(named: "back_ar_ic")
        } else {
            backButton.image = UIImage(named: "back_en_ic")
        }
        
        self.navigationItem.title = " \(ticket.ticketId!)"
    }
    
    func getRelatedTickets() {
        performSegue(withIdentifier: "relatedTicket", sender: self)
    }

    @objc func backAction(_ sender: Any) {
        if(!isFromNotification){
            dismiss(animated: true, completion: nil)
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let destinationNavigationController = storyboard.instantiateViewController(withIdentifier: "navigationVC") as! NavigationVC
//            let targetController = destinationNavigationController.topViewController as! HomeVC
            self.present(destinationNavigationController, animated: true, completion: nil)
        }
        //self.navigationController?.popViewController(animated: true)
    }
    @objc func sharePressed()
    {
        let actionSheet = UIAlertController(title: "", message: "Share your Note", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let tweetAction = UIAlertAction(title: "Share on Twitter", style: UIAlertActionStyle.default) { (action) -> Void in
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) {
                // Initialize the default view controller for sharing the post.
                let twitterComposeVC = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                
                // Set the note text as the default post message.
                if (self.ticket.desc?.characters.count)! <= 140 {
                    twitterComposeVC?.setInitialText("\(self.ticket.desc ?? "")")
                    
                }
                else {
                    let string1 = self.ticket.desc
                    
                    let index1 = string1?.index((string1?.endIndex)!, offsetBy: 140)
                    let substring1 = string1?.substring(to: index1!)
                    //let substring1 = String(string1?[..<index1])
                    twitterComposeVC?.setInitialText("\(substring1 ?? "")")
                    
                }
                self.present(twitterComposeVC!, animated: true, completion: nil)
            }
            else {
                UIApplication.shared.openURL(NSURL(string: "http://www.twitter.com")! as URL)
            }
        }
        
        
        // Configure a new action to share on Facebook.
        let facebookPostAction = UIAlertAction(title: "Share on Facebook", style: UIAlertActionStyle.default) { (action) -> Void in
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
                let facebookComposeVC = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                
                facebookComposeVC?.add(URL(string: self.ticket.imagesUrl![0]))
               // facebookComposeVC?.setInitialText("Your ticket via Sharek: " + self.ticket.desc!)
                //facebookComposeVC?.accessibilityLabel = "knhlk.nk"
                facebookComposeVC?.title = "Your ticket via Sharek: " + self.ticket.desc!
                
//                facebookComposeVC?.add(UIImage(named: "m.png"))
                self.present(facebookComposeVC!, animated: true, completion: nil)
                
            }
            else {
               self.showAlertMessage(message: "You are not connected to your Facebook account.")
            }
        }
        
        // Configure a new action to show the UIActivityViewController
        let moreAction = UIAlertAction(title: "More", style: UIAlertActionStyle.default) { (action) -> Void in
            let activityViewController = UIActivityViewController(activityItems: ["Your ticket via Sharek: " + self.ticket.desc!], applicationActivities: nil)
            
            self.present(activityViewController, animated: true, completion: nil)
        }
        
        
        let dismissAction = UIAlertAction(title: "Close", style: UIAlertActionStyle.cancel) { (action) -> Void in
            
        }
        
        
        actionSheet.addAction(tweetAction)
        actionSheet.addAction(facebookPostAction)
        actionSheet.addAction(moreAction)
        actionSheet.addAction(dismissAction)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func showAlertMessage(message: String!) {
        let alertController = UIAlertController(title: "EasyShare", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}

extension TicketDetailsVC : UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (ticket.imagesUrl?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: DetailsCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "postImageCell", for: indexPath) as! DetailsCollectionCell
        
        cell.setCell(ticket: ticket,indexPath: indexPath)
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        item = indexPath.row
       performSegue(withIdentifier: "detailsSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailsSegue" {
            let destinationController = segue.destination as! ImageDetailsVC
         
            destinationController.index = item
            destinationController.ticket = ticket
        }else if segue.identifier == "relatedTicket"
        {
            let destinationController = segue.destination as! RelatedTicketsVC
            
            destinationController.ticket = ticket
            
        }
    }
    
}
