//
//  RelatedTicketsVC.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/17/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit
import MBProgressHUD
import Toaster

class RelatedTicketsVC: UITableViewController, RelatedTicketsView {

    var tickets = [Ticket]()
    var ticket = Ticket()
    var presenter :RelatedTicketsPresenter!
    var loading: MBProgressHUD!
    var selectedTicket = Ticket()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(ticket.ticketId ?? "")
        presenter = RelatedTicketsPresenter(repository: Injection.provideNetworkTickettRepository())
        presenter.setView(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        presenter.getRelatedTickets(ticketId:String( ticket.ticketId!))
    }

  

    override func numberOfSections(in tableView: UITableView) -> Int {
      
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return tickets.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PostCell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostCell
        cell.selectionStyle = .none
        //tableViewCell =  tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostCell
        
        
            ticket = tickets[indexPath.row]
            cell.ticketLabel.textColor = UIColor.blue
        
        
        
        
        if(ticket.ticketEmotionId == 0)
        {
            cell.emoImageView.image = UIImage(named: "shoked_ic.png")
        }else if(ticket.ticketEmotionId == 1)
        {
            cell.emoImageView.image = UIImage(named: "happy_ic.png")
        }else{
            cell.emoImageView.image = UIImage(named: "angry_ic.png")
        }
        
        cell.ticketLabel.text = ticket.desc
        
        if((ticket.replay?.count)! > 0)
        {
            cell.responseLabel.text = ticket.replay?[0].replay
        }else{
            
            cell.responseLabel.text = ""
        }
        
        
        let formatter = DateFormatter()
        formatter.calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.ISO8601)! as Calendar
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
        formatter.dateFormat = "hh:mm a"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-ddEHH:mm:ssZ" //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        // print(ticket.ticketDate)
        let date = dateFormatter.date(from: ticket.ticketDate!) //according to date format your date string
        print(date ?? "")
        let dateString = formatter.string(from: date!)
        print(dateString)
        
        cell.dateLabel.text = dateString
        
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
        
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //performSegue(withIdentifier: "detailsSegue", sender: self)
    }

    func showloading() {
        loading = MBProgressHUD.showAdded(to: self.view, animated: true)
        loading.mode = MBProgressHUDMode.indeterminate
    }
    
    func hideLoading() {
        
        if(loading != nil) {
            loading.hide(animated: true)
            loading = nil
        }
    }

    
    func showNetworkError(){
        Toast(text: "connectionFailed".localized(), duration: Delay.short).show()
        
        
    }

    func showServerError() {
        Toast(text: "error".localized(), duration: Delay.short).show()
    }
    

    
    func setData(tickets : [Ticket]){
        
        self.tickets = tickets
        tableView.reloadData()
    }
    func setNoData(){
        Toast(text: "no_data".localized(), duration: Delay.short).show()

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if ( segue.identifier == "detailsSegue")
        {
            var tableIndexPath = self.tableView.indexPathForSelectedRow
            selectedTicket = tickets[(tableIndexPath?.row)!]
            let detailsVC = segue.destination as! TicketDetailsVC
            detailsVC.ticket = self.selectedTicket
        }
    }



}
extension RelatedTicketsVC : UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
    
        
         return (ticket.imagesUrl?.count)!
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PostCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "postImageCell", for: indexPath) as! PostCollectionCell
        
               let url = URL(string: (ticket.imagesUrl?[indexPath.row])!)
        
        cell.ticketImageView.kf.setImage(with: url, placeholder: UIImage(named : "pic.png"), options: nil, progressBlock: nil, completionHandler: nil)

        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //performSegue(withIdentifier: "detailsSegue", sender: self)
    }
    
    
    
}
