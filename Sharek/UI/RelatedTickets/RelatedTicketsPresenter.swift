//
//  RelatedTicketsPresenter.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/17/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation
import UIKit

class RelatedTicketsPresenter : Presenter {
    
    
    var view : RelatedTicketsView?
    var ticketRespository: TicketRepository!
    
    
    init (repository: TicketRepository) {
        ticketRespository = repository
        
    }
    
    func setView(_ view: RelatedTicketsView) {
        weak var weakView = view
        self.view = weakView
    }
    
    func getRelatedTickets(ticketId : String)  {
        view?.showloading()
        ticketRespository.getRelatedTickets(baseTicketId: ticketId) { (result, error) in
            self.view?.hideLoading()
            if(error == NetworkTicketRepository.ErrorType.none)
            {
                
                
                if(result.isEmpty)
                {
                    self.view?.setNoData()
                }else{
                    self.view?.setData(tickets: result)
                }
            }else if error == NetworkTicketRepository.ErrorType.serverError
            {
                self.view?.showServerError()
                print("server error")
            }else{
                
                self.view?.showNetworkError()
                print("network failure")
            }
        }
        }
    
   
    func didLoad(){}
    func didAppear(){}
}
protocol RelatedTicketsView : class{
    
    
    func showloading()
    func hideLoading()
    func showNetworkError()
    func showServerError()
    func setData(tickets : [Ticket])
    func setNoData()
    
    
    
}
