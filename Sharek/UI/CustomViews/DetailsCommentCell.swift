//
//  DetailsCommentCell.swift
//  Sharek
//
//  Created by Salma on 8/2/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit

class DetailsCommentCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var adminImage: UIButton!
    
    var relatedTickets : RelatedTicketsProtocol!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCell(ticket : Ticket , indexPath : IndexPath,relatedTicketsDelegete : RelatedTicketsProtocol , isFromNotification :Bool?)
    {
        self.relatedTickets = relatedTicketsDelegete

        dateLabel.text = getDateAsString(ticket: ticket , isFromNotification : isFromNotification)
        responseLabel.text = ticket.replay?[0].replay
    }
    
    func getDateAsString(ticket : Ticket ,isFromNotification :Bool?) -> String
    {
        let formatter = DateFormatter()
        formatter.calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.ISO8601)! as Calendar
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
        formatter.dateFormat = "dd MMM, hh:mm a"
        
        let dateFormatter = DateFormatter()
        if(!isFromNotification!){
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        }else{
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Your date format

        }
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        // print(ticket.ticketDate)
        let date = dateFormatter.date(from: ticket.ticketDate!) //according to date format your date string
        print(date ?? "")
        let dateString = formatter.string(from: date!)
        return dateString
    }
    
    
    @IBAction func adminIconPressed(_ sender: Any) {
        
        relatedTickets.getRelatedTickets()
    }

}

protocol RelatedTicketsProtocol {
    func getRelatedTickets()
}

