//
//  PostCell.swift
//  Sharek
//
//  Created by Salma on 8/2/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {

    @IBOutlet weak var ticketLabel: UILabel!
    @IBOutlet weak var emoImageView: UIImageView!
    @IBOutlet weak var responseLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(_ ticket : Ticket) {
        
        
        if(ticket.ticketEmotionId == 0)
        {
            emoImageView.image = UIImage(named: "shoked_ic.png")
        }else if(ticket.ticketEmotionId == 1)
        {
            emoImageView.image = UIImage(named: "happy_ic.png")
        }else{
            emoImageView.image = UIImage(named: "angry_ic.png")
        }
        
        ticketLabel.text = ticket.desc
        
        if((ticket.replay?.count)! > 0)
        {
            responseLabel.text = ticket.replay?[0].replay
        }else{
            
            responseLabel.text = ""
        }
        
//        if ticket.imagesUrl?.count != nil {
//            //imagesCount.append(ticket.imagesUrl!.count)
//        }
        
        
        let formatter = DateFormatter()
        formatter.calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.ISO8601)! as Calendar
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
        formatter.dateFormat = "hh:mm a"
        
        let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-ddEHH:mm:ssZ" //Your date format
        
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        // print(ticket.ticketDate)
        let date = dateFormatter.date(from: ticket.ticketDate!) //according to date format your date string
        print(date ?? "")
        let dateString = formatter.string(from: date!)
        print(dateString)
        
        dateLabel.text = dateString
       
        
    }


}
