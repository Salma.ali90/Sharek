//
//  DetailsCollectionCell.swift
//  Sharek
//
//  Created by Salma on 8/2/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit

class DetailsCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var ticketImageView: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        ticketImageView.layer.cornerRadius = 5
        ticketImageView.clipsToBounds = true
        
    }
    
    
    func setCell(ticket :Ticket, indexPath : IndexPath){
        let url = URL(string: (ticket.imagesUrl?[indexPath.row])!)
        ticketImageView.kf.setImage(with: url, placeholder: UIImage(named : "loader"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    
}
