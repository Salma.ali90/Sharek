//
//  DetailsInfoCell.swift
//  Sharek
//
//  Created by Salma on 8/2/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit

class DetailsInfoCell: UITableViewCell {

    @IBOutlet weak var ticketLabel: UILabel!
    @IBOutlet weak var emoImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(ticket : Ticket , isFromNotification : Bool) {
        
        dateLabel.text = getDateAsString(ticket: ticket , isFromNotification: isFromNotification)
        locationLabel.text = ticket.Location
        ticketLabel.text = ticket.desc
        if(ticket.ticketEmotionId == 0)
        {
            emoImageView.image = UIImage(named: "shoked_ic.png")
        }else if(ticket.ticketEmotionId == 1)
        {
            emoImageView.image = UIImage(named: "happy_ic.png")
        }else{
            emoImageView.image = UIImage(named: "angry_ic.png")
        }

    }
    
    func getDateAsString(ticket : Ticket, isFromNotification : Bool) -> String
    {
        let formatter = DateFormatter()
        formatter.calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.ISO8601)! as Calendar
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
        formatter.dateFormat = "dd MMM, hh:mm a"
        
        let dateFormatter = DateFormatter()
        if(!isFromNotification){
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ" //Your date format
        }else{
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Your date format
        }
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        // print(ticket.ticketDate)
        let date = dateFormatter.date(from: ticket.ticketDate!) //according to date format your date string
        print(date ?? "")
        let dateString = formatter.string(from: date!)
        return dateString
    }


}
