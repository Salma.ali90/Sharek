//
//  ImageDetailsPresenter.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/16/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import Foundation

import UIKit

class ImageDetailsPresenter: Presenter {
    
    
    var view : ImageDetailsView?
    var ticketRespository: TicketRepository!
    
    
    init (repository: TicketRepository) {
        ticketRespository = repository
        
    }
    
    func setView(_ view: ImageDetailsView) {
        weak var weakView = view
        self.view = weakView
        
}
    func deleteImage(ImageId : String)
    {
        view?.showloading()
        
        ticketRespository.deleteImage(imageId: ImageId) { (result, error) in
            self.view?.hideLoading()
            if(error == NetworkTicketRepository.ErrorType.none) {
                
                print(result)
                // UPLOAD SUCCESSSFULLY
                print("image deleted")
                self.view?.showSuccess()
            }else if(error == NetworkTicketRepository.ErrorType.network)
            {
                self.view?.showNetworkError()
            }else {
                self.view?.showServerError()
                print("error")
            }
        }
    }
    func didLoad(){}
    func didAppear(){}
}
protocol ImageDetailsView : class{
    
    
    func showloading()
    func hideLoading()
    func showNetworkError()
    func showServerError()
    func showSuccess()
   
    
    }
