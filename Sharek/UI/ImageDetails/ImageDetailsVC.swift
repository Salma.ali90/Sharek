//
//  ImageDetailsVC.swift
//  Sharek
//
//  Created by Salma Abd Elazim on 8/9/17.
//  Copyright © 2017 Sharek. All rights reserved.
//

import UIKit
import Toaster
import MBProgressHUD

class ImageDetailsVC: UIViewController , ImageDetailsView {

    
    var appdelegate = UIApplication.shared.delegate as! AppDelegate
    var index : Int = 0
    var ticket = Ticket()
    var presenter : ImageDetailsPresenter!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var removeView: UIView!
    @IBOutlet weak var ticketID: UILabel!
    
    @IBOutlet weak var removeIcon: UIButton!
    
    @IBOutlet weak var backIcon: UIButton!
    var loading: MBProgressHUD!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = ImageDetailsPresenter(repository: Injection.provideNetworkTickettRepository())
        presenter.setView(self)
        intialization()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    func intialization()
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backPressed(_:)))
        
        closeView.addGestureRecognizer(tap)
        
        closeView.isUserInteractionEnabled = true
        
        let removeTab = UITapGestureRecognizer(target: self, action: #selector(self.removePressed(_:)))
        
        removeView.addGestureRecognizer(removeTab)
        removeView.isUserInteractionEnabled = true
        
        
        if ticket.ticketId != 0 {
            self.ticketID.text = String(describing: ticket.ticketId!)
            self.removeIcon.isHidden = true
            self.removeView.isHidden = true
            let url = URL(string: (ticket.imagesUrl?[index])!)
            imageView.kf.setImage(with: url, placeholder: UIImage(named : "loader"), options: nil, progressBlock: nil, completionHandler: nil)
            
        }else{
            imageView.image=appdelegate.images[index]
        }
        
        if(appdelegate.isRTL) {
            backIcon.setImage(UIImage(named: "back_ar_ic"), for: .normal)
            
            
        } else {
            backIcon.setImage(UIImage(named: "back_en_ic"), for: .normal)
        }


        
       
    }
    
    @objc func backPressed(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func cancelPressed(_ sender: UITapGestureRecognizer)
    {
        self.dismiss(animated: true, completion: nil)
    }
   
    @IBAction func backIconPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func removePressed(_ sender: UITapGestureRecognizer)
    {
        DeleteAction()
    }
    @IBAction func trashPressed(_ sender: Any) {
        DeleteAction()
    }
    
    func DeleteAction()
    {
        presenter.deleteImage(ImageId: String(describing: appdelegate.imagesId[index]))
    }
    
    func showloading()
    {
        loading = MBProgressHUD.showAdded(to: self.view, animated: true)
        loading.mode = MBProgressHUDMode.indeterminate
    }
    
    func hideLoading()
    {
        
        if(loading != nil) {
            loading.hide(animated: true)
            loading = nil
        }
    }
    func showNetworkError(){
        
        Toast(text: "connectionFailed".localized(), duration: Delay.short).show()
        
    }
       func showServerError(){
        Toast(text: "error".localized(), duration: Delay.short).show()
        
    }
    func showSuccess(){
        appdelegate.images.remove(at: index)
        appdelegate.counter -= 1
        appdelegate.imagesId.remove(at: index)
        self.dismiss(animated: true, completion: nil)
        Toast(text: "ImageDeleted".localized(), duration: Delay.short).show()
    }

    

}
