//
//  NavigationVC.swift
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//


import UIKit
import SideMenuController

class NavigationVC: SideMenuController {

    var appDelegate: AppDelegate!
    static var actionTo = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        appDelegate = UIApplication.shared.delegate as! AppDelegate

        if(appDelegate.isRTL) {
            SideMenuController.preferences.drawing.sidePanelPosition = .underCenterPanelRight
            
        } else {
            SideMenuController.preferences.drawing.sidePanelPosition = .underCenterPanelLeft
        }
 
        //SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "sidemenu")
        SideMenuController.preferences.drawing.sidePanelWidth = 250

        performSegue(withIdentifier: "initialCenterController", sender: nil)
        performSegue(withIdentifier: "sideController", sender: nil)

        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

   

}
