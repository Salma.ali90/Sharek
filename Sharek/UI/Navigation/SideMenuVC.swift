//
//  SideMenuVC.swift
//  IAAF
//
//  Created by Salma Ali on 7/28/17.
//  Copyright © 2017 Sharek. All rights reserved.
//


import UIKit
import Kingfisher

class SideMenuVC: UIViewController {
    
    @IBOutlet weak var participationLabel: UILabel!
    @IBOutlet weak var settingLabel: UILabel!
    @IBOutlet weak var aboutLabel: UILabel!
    
    @IBOutlet weak var participationView: UIView!
    @IBOutlet weak var settingView: UIView!
    @IBOutlet weak var aboutView: UIView!
    
    var userDefault: UserDefault!
    var appDelegate: AppDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let backgroundImage = UIImage(named: "sidemenu_background_2")
       //let imageView = UIImageView(image: backgroundImage)
       // self.tableView.backgroundView = imageView
        
        userDefault = UserDefault()
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        localizeStrings()
        //setUI()
        setFonts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setData()
    }
 
    
    func localizeStrings() {
        participationLabel.text = "menuParticipation".localized()
        settingLabel.text = "menuSettings".localized()
        aboutLabel.text = "menuAbout".localized()
    }
    
    func setData() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.participationTap(_:)))
        participationView.addGestureRecognizer(tap)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.settingTap(_:)))
        settingView.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.aboutTap(_:)))
        aboutView.addGestureRecognizer(tap3)
        
    }
    
    
    @objc func participationTap(_ sender: UITapGestureRecognizer) {
        sideMenuController?.performSegue(withIdentifier: "initialCenterController", sender: self)
    }
    
    @objc func settingTap(_ sender: UITapGestureRecognizer) {
        sideMenuController?.performSegue(withIdentifier: "settingsSegue", sender: self)
    }
    
    @objc func aboutTap(_ sender: UITapGestureRecognizer) {
        sideMenuController?.performSegue(withIdentifier: "aboutSegue", sender: self)
    }
    
    func setFonts() {


    }
    
    @IBAction func facebookPressed(_ sender: Any) {
        
        UIApplication.shared.openURL(NSURL(string: "https://www.facebook.com/")! as URL)
    }
    
    
    @IBAction func twitterPressed(_ sender: Any) {
        
        UIApplication.shared.openURL(NSURL(string: "https://twitter.com/")! as URL)
    }
    
    @IBAction func instagramPressed(_ sender: Any) {
        
         UIApplication.shared.openURL(NSURL(string: "https://www.instagram.com/")! as URL)
    }
}
